#!/usr/bin/env bash
echo "Installing Node packages via Yarn..."
yarn install

echo "Checking commits"
npx commitlint-gitlab-ci -x @commitlint/config-conventional

echo "Checking release"
scripts/check-release

echo "Building everything"
scripts/build-all
