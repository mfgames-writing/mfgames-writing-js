#!/usr/bin/env bash

yarn install

scripts/check-release
scripts/build-all
scripts/run-release
