# [@mfgames-writing/format-v1.6.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.5.0...format-1.6.0) (2021-07-24)

# [@mfgames-writing/format-v1.5.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.4.0...format-v1.5.0) (2021-07-17)

# [@mfgames-writing/format-v1.4.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.3.9...format-v1.4.0) (2021-07-17)

# [@mfgames-writing/format-v1.3.9](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.3.8...format-v1.3.9) (2021-07-17)

# [@mfgames-writing/format-v1.3.8](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.3.7...format-v1.3.8) (2021-07-17)

# [@mfgames-writing/format-v1.3.7](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.3.6...format-v1.3.7) (2021-03-15)

# [@mfgames-writing/format-v1.3.6](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.3.5...format-v1.3.6) (2021-03-07)

# [@mfgames-writing/format-v1.3.5](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.3.4...format-v1.3.5) (2021-03-06)

# [@mfgames-writing/format-v1.3.4](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.3.3...format-v1.3.4) (2021-03-06)

# [@mfgames-writing/format-v1.3.3](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.3.2...format-v1.3.3) (2021-03-06)

# [@mfgames-writing/format-v1.3.2](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.3.1...format-v1.3.2) (2021-03-05)

# [@mfgames-writing/format-v1.3.1](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.3.0...format-v1.3.1) (2021-02-04)

# [@mfgames-writing/format-v1.3.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/format-v1.2.4...format-v1.3.0) (2021-02-03)


### Bug Fixes

* correcting mistakes from monorepo merging and refactoring ([7d44c63](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/7d44c632a51a05a69bb1cd8dbb81a8b603a6d0d1))


### Features

* exceptions thrown while formatting return non-zero to shell ([ac94131](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/ac94131d20cfde1f87e09397bd29bcabaa9919a1))

# [@mfgames-writing/format-v1.2.4](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/format-v1.2.3...format-v1.2.4) (2020-07-26)


### Bug Fixes

* fixed parameters for yaml-front-matter calls ([f3d3868](https://git.sr.ht/~dmoonfire/mfgames-writing-js/commit/f3d3868a87a13dbd3cc596f56d329e44929b8180))
* **format:** fixing a possible undefined in cli.ts ([d562271](https://git.sr.ht/~dmoonfire/mfgames-writing-js/commit/d56227147a90a07bd55cb0a0b6aa484066975236))

# [@mfgames-writing/format-v1.1.2](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/format-v1.1.1...format-v1.1.2) (2020-07-26)


### Bug Fixes

* fixed parameters for yaml-front-matter calls ([f3d3868](https://git.sr.ht/~dmoonfire/mfgames-writing-js/commit/f3d3868a87a13dbd3cc596f56d329e44929b8180))
* **format:** fixing a possible undefined in cli.ts ([d562271](https://git.sr.ht/~dmoonfire/mfgames-writing-js/commit/d56227147a90a07bd55cb0a0b6aa484066975236))

## [1.1.1](https://gitlab.com/mfgames-writing/mfgames-writing-format-js/compare/v1.1.0...v1.1.1) (2018-08-11)


### Bug Fixes

* switching to package management ([9f6d460](https://gitlab.com/mfgames-writing/mfgames-writing-format-js/commit/9f6d460))
