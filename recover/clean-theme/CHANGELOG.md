# [@mfgames-writing/clean-theme-v3.4.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.3.0...clean-theme-3.4.0) (2021-07-24)

# [@mfgames-writing/clean-theme-v3.3.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.2.0...clean-theme-v3.3.0) (2021-07-17)

# [@mfgames-writing/clean-theme-v3.2.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.1.9...clean-theme-v3.2.0) (2021-07-17)

# [@mfgames-writing/clean-theme-v3.1.9](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.1.8...clean-theme-v3.1.9) (2021-07-17)

# [@mfgames-writing/clean-theme-v3.1.8](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.1.7...clean-theme-v3.1.8) (2021-07-17)

# [@mfgames-writing/clean-theme-v3.1.7](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.1.6...clean-theme-v3.1.7) (2021-03-15)

# [@mfgames-writing/clean-theme-v3.1.6](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.1.5...clean-theme-v3.1.6) (2021-03-07)

# [@mfgames-writing/clean-theme-v3.1.5](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.1.4...clean-theme-v3.1.5) (2021-03-06)

# [@mfgames-writing/clean-theme-v3.1.4](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.1.3...clean-theme-v3.1.4) (2021-03-06)

# [@mfgames-writing/clean-theme-v3.1.3](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.1.2...clean-theme-v3.1.3) (2021-03-06)

# [@mfgames-writing/clean-theme-v3.1.2](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.1.1...clean-theme-v3.1.2) (2021-03-05)

# [@mfgames-writing/clean-theme-v3.1.1](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.1.0...clean-theme-v3.1.1) (2021-02-04)

# [@mfgames-writing/clean-theme-v3.1.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/clean-theme-v3.0.13...clean-theme-v3.1.0) (2021-02-03)


### Bug Fixes

* correcting mistakes from monorepo merging and refactoring ([7d44c63](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/7d44c632a51a05a69bb1cd8dbb81a8b603a6d0d1))

# [@mfgames-writing/clean-theme-v3.0.13](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/clean-theme-v3.0.12...clean-theme-v3.0.13) (2020-07-26)

## [3.0.5](https://gitlab.com/mfgames-writing/mfgames-writing-clean-js/compare/v3.0.4...v3.0.5) (2018-08-12)


### Bug Fixes

* removing break-word CSS tags ([943c3ad](https://gitlab.com/mfgames-writing/mfgames-writing-clean-js/commit/943c3ad))

## [3.0.4](https://gitlab.com/mfgames-writing/mfgames-writing-clean-js/compare/v3.0.3...v3.0.4) (2018-08-12)


### Bug Fixes

* explict alignment and word break for p, i, em, b, and strong ([a4d7283](https://gitlab.com/mfgames-writing/mfgames-writing-clean-js/commit/a4d7283))

## [3.0.3](https://gitlab.com/mfgames-writing/mfgames-writing-clean-js/compare/v3.0.2...v3.0.3) (2018-08-09)


### Bug Fixes

* fixing issues with packages and paths ([636f504](https://gitlab.com/mfgames-writing/mfgames-writing-clean-js/commit/636f504))
