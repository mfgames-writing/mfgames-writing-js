# EPUB Formatter Moonfire Games Writing

This plugin is used to create EPUB documents for Moonfire Games Writing Formatter.

## Including Assets

To include an asset into the EPUB, such as a font, use the following:

```yaml
editions:
  epub:
    format: "@mfgames-writing/epub2"
    assets:
      - path: relative/path/to/font.ttf
        archivePath: assets/font.ttf
        mime: font/ttf

```

In the above example, the path to reference inside the EPUB is "assets/font.ttf". `path` can be used to pull in a font from `node_modules/` if desired.
