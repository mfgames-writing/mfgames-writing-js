# [@mfgames-writing/liquid-v1.4.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.3.0...liquid-1.4.0) (2021-07-24)


### Features

* **liquid:** switch from `node-sass` to `sass` ([65a4bc0](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/65a4bc037916a60d0630bee36ceb57d77f85deb4))

# [@mfgames-writing/liquid-v1.3.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.2.0...liquid-v1.3.0) (2021-07-17)

# [@mfgames-writing/liquid-v1.2.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.1.9...liquid-v1.2.0) (2021-07-17)

# [@mfgames-writing/liquid-v1.1.9](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.1.8...liquid-v1.1.9) (2021-07-17)

# [@mfgames-writing/liquid-v1.1.8](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.1.7...liquid-v1.1.8) (2021-07-17)

# [@mfgames-writing/liquid-v1.1.7](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.1.6...liquid-v1.1.7) (2021-03-15)

# [@mfgames-writing/liquid-v1.1.6](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.1.5...liquid-v1.1.6) (2021-03-07)

# [@mfgames-writing/liquid-v1.1.5](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.1.4...liquid-v1.1.5) (2021-03-06)

# [@mfgames-writing/liquid-v1.1.4](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.1.3...liquid-v1.1.4) (2021-03-06)

# [@mfgames-writing/liquid-v1.1.3](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.1.2...liquid-v1.1.3) (2021-03-06)

# [@mfgames-writing/liquid-v1.1.2](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.1.1...liquid-v1.1.2) (2021-03-05)

# [@mfgames-writing/liquid-v1.1.1](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.1.0...liquid-v1.1.1) (2021-02-04)

# [@mfgames-writing/liquid-v1.1.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/liquid-v1.0.14...liquid-v1.1.0) (2021-02-03)


### Bug Fixes

* correcting mistakes from monorepo merging and refactoring ([7d44c63](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/7d44c632a51a05a69bb1cd8dbb81a8b603a6d0d1))

# [@mfgames-writing/liquid-v1.0.14](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/liquid-v1.0.13...liquid-v1.0.14) (2020-07-26)


### Bug Fixes

* switched out dotted for lodash.get properly ([ae6cdd0](https://git.sr.ht/~dmoonfire/mfgames-writing-js/commit/ae6cdd071b440f23ba825fb06fe71ad16f9923a2))

## [1.0.6](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/compare/v1.0.5...v1.0.6) (2018-08-09)


### Bug Fixes

* fixing tsconfig.json build generation ([ff6ac12](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/commit/ff6ac12))

## [1.0.5](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/compare/v1.0.4...v1.0.5) (2018-08-09)


### Bug Fixes

* adding missing publish npm task to semantic-release ([d52f56f](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/commit/d52f56f))

## [1.0.4](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/compare/v1.0.3...v1.0.4) (2018-08-09)


### Bug Fixes

* trying to get ci to build cleanly ([ef34e69](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/commit/ef34e69))

## [1.0.3](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/compare/v1.0.2...v1.0.3) (2018-08-09)


### Bug Fixes

* fixing package references ([d1736a0](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/commit/d1736a0))

## [1.0.2](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/compare/v1.0.1...v1.0.2) (2018-08-09)


### Bug Fixes

* fixing bad package.json entry points ([a5d4126](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/commit/a5d4126))
