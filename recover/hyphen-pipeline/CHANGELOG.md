# [@mfgames-writing/hyphen-pipeline-v0.8.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.7.0...hyphen-pipeline-0.8.0) (2021-07-24)

# [@mfgames-writing/hyphen-pipeline-v0.7.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.6.0...hyphen-pipeline-v0.7.0) (2021-07-17)

# [@mfgames-writing/hyphen-pipeline-v0.6.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.5.9...hyphen-pipeline-v0.6.0) (2021-07-17)

# [@mfgames-writing/hyphen-pipeline-v0.5.9](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.5.8...hyphen-pipeline-v0.5.9) (2021-07-17)


### Bug Fixes

* **hyphen-pipeline:** also do not hyphenate image links ([8a7ce22](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/8a7ce22fd0eb632717245a331c3473359ac0c49b))

# [@mfgames-writing/hyphen-pipeline-v0.5.8](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.5.7...hyphen-pipeline-v0.5.8) (2021-07-17)


### Bug Fixes

* **hyphen-pipeline:** do not hyphenate URLs ([7b9b7a7](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/7b9b7a7869f8624050ef4c8f8fccd7d8d57b8457))

# [@mfgames-writing/hyphen-pipeline-v0.5.7](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.5.6...hyphen-pipeline-v0.5.7) (2021-03-15)

# [@mfgames-writing/hyphen-pipeline-v0.5.6](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.5.5...hyphen-pipeline-v0.5.6) (2021-03-07)


### Bug Fixes

* **hyphen-pipeline:** hyphen requires an array, not a string ([e8eb44a](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/e8eb44a97d38361f5a7e20557855e4a876566e1e))

# [@mfgames-writing/hyphen-pipeline-v0.5.5](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.5.4...hyphen-pipeline-v0.5.5) (2021-03-06)

# [@mfgames-writing/hyphen-pipeline-v0.5.4](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.5.3...hyphen-pipeline-v0.5.4) (2021-03-06)

# [@mfgames-writing/hyphen-pipeline-v0.5.3](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.5.2...hyphen-pipeline-v0.5.3) (2021-03-06)

# [@mfgames-writing/hyphen-pipeline-v0.5.2](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.5.1...hyphen-pipeline-v0.5.2) (2021-03-05)

# [@mfgames-writing/hyphen-pipeline-v0.5.1](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.5.0...hyphen-pipeline-v0.5.1) (2021-02-04)

# [@mfgames-writing/hyphen-pipeline-v0.5.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/hyphen-pipeline-v0.4.9...hyphen-pipeline-v0.5.0) (2021-02-03)


### Bug Fixes

* correcting mistakes from monorepo merging and refactoring ([7d44c63](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/7d44c632a51a05a69bb1cd8dbb81a8b603a6d0d1))

# [@mfgames-writing/hyphen-v0.4.10](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/hyphen-v0.4.9...hyphen-v0.4.10) (2020-07-26)
