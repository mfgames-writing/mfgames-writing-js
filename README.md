Moonfire Games' Writing Tools
=============================

The `mfgames-writing` is a set of tools and utilities for working with novels,
stories, and related prose written in Markdown files with a YAML header. It
includes functionality to compile the results into PDF, EPUB, MOBI, and HTML
formats and is well suited for working with continual integration servers.

This is the Javascript edition of the `mfgames-writing` tools. There have been
many iterations of those tools over the years, but this is the currently
maintained one.
