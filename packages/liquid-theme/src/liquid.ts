import {
    ContentArgs,
    ContentTheme,
    EditionArgs,
    Theme,
    PandocThemeSettings,
} from "@mfgames-writing/contracts";
import * as glob from "glob";
import * as _ from "lodash";
import * as liquid from "liquid-node";
import * as fs from "mz/fs";
import * as sass from "sass";
import * as path from "path";
import * as yamlFrontMatter from "yaml-front-matter";
const anySass: any = sass;

export class LiquidTheme implements Theme {
    public stylesheetFileName: string = "stylesheet.css";
    public templateDirectory: string;
    public styleDirectory: string;
    public assetDirectory: string | undefined;
    public pandocSettings: PandocThemeSettings | undefined;
    private templates: { [index: string]: any } = {};

    constructor(
        templateDirectory: string,
        styleDirectory: string,
        assetDirectory: string | undefined = undefined
    ) {
        this.templateDirectory = templateDirectory;
        this.styleDirectory = styleDirectory;
        this.assetDirectory = assetDirectory;
    }

    public start(args: EditionArgs): Promise<EditionArgs> {
        // Report what we're doing.
        args.logger.debug(`Using liquid template: ${this.templateDirectory}`);
        args.logger.debug(`Using liquid style: ${this.styleDirectory}`);
        args.logger.debug(`Using liquid asset: ${this.assetDirectory}`);

        // Figure out the path for our templates.
        let templates = glob.sync(path.join(this.templateDirectory, "*.xhtml"));
        let promise: Promise<any> = Promise.resolve(args);

        // Loop through the files and add each one.
        for (let template of templates) {
            // Figure out the base name.
            let basename = path.basename(template, ".xhtml");

            // Load the file into memory, split out the YAML header, and put
            // the text into "contents".
            promise = promise.then(() => fs.readFile(template));
            promise = promise.then((buffer) => {
                let metadata = yamlFrontMatter.loadFront(buffer, {
                    contentKeyName: "_contents",
                });
                this.templates[basename] = metadata;
                return args;
            });
        }

        // Return the resulting promise chain.
        promise = promise.then(() => Promise.resolve(args));
        return promise;
    }

    public finish(args: EditionArgs): Promise<EditionArgs> {
        return Promise.resolve(args);
    }

    public renderHtml(content: ContentArgs): Promise<ContentArgs> {
        return this.renderTemplateChain(content, content.element, "html");
    }

    public renderLayout(content: ContentArgs): Promise<ContentArgs> {
        return this.renderTemplateChain(content, "default", "default");
    }

    public renderNavigationTitle(args: ContentArgs): string {
        if (args.metadata.number !== undefined) {
            switch (args.element) {
            case "chapter":
                return `Chapter ${args.metadata.number}: ${args.metadata.title}`;
            case "appendix":
                return `Appendix ${args.metadata.number}: ${args.metadata.title}`;
            }
        }

        return args.metadata.title;
    }

    public renderStylesheet(
        args: EditionArgs,
        mode?: string | string[]
    ): Buffer {
        // Normalize the modes for the stylesheets and append the fallback.
        mode = mode ? mode : [];

        if (typeof mode === "string") {
            mode = [mode];
        }

        mode.push("stylesheet");

        // Figure out which stylesheet to use based on the modes.
        let fileName: string | undefined = undefined;

        for (var testMode of mode) {
            fileName = path.join(this.styleDirectory, `${testMode}.scss`);

            if (fs.existsSync(fileName)) {
                break;
            }
        }

        if (!fileName) {
            throw new Error("Cannot find style sheet: " + JSON.stringify(mode));
        }

        // Render the SASS file and return the results.
        args.logger.info(
            `Using stylesheet: ${fileName.replace(this.styleDirectory, "")}`
        );
        let results = sass.renderSync({
            file: fileName,
            functions: {
                "theme($key, $fallback: \"\")": function (
                    key: any,
                    fallback: any
                ) {
                    // Retrieve the variable from our system.
                    let value = _.get(args, key.getValue());

                    if (value === undefined) {
                        value = fallback;
                    }

                    // Pass the data back to the SASS system.
                    var results = new anySass.types.String(
                        `${value.toString()}`
                    );
                    return results;
                },
                "themeString($key, $fallback: \"\")": function (
                    key: any,
                    fallback: any
                ) {
                    // Retrieve the variable from our system.
                    let value = _.get(args, key.getValue());

                    if (value === undefined) {
                        value = fallback;
                    }

                    // Pass the data back to the SASS system.
                    var results = new anySass.types.String(
                        `"${value.toString()}"`
                    );
                    return results;
                },
            },
        });

        // See if we have additional SASS to include.
        let additionalCss: string = args.edition.style?.css ?? "";
        const stylesheetCss = results.css.toString();
        const combinedCss = stylesheetCss + additionalCss;

        return Buffer.from(combinedCss);
    }

    public renderRule(_args: ContentArgs): string {
        return "<hr />";
    }

    public renderTableOfContents(args: ContentArgs): Promise<ContentArgs> {
        return new Promise<ContentArgs>((resolve) => {
            // Generate the TOC.
            args.logger.trace("Rendering table of contents");

            // Figure out which contents we need in the TOC.
            var tocContents = args.editionArgs.contents
                .filter(
                    (c) => !c.contentData.exclude || !c.contentData.exclude.toc
                )
                .filter((c) => c.element !== "toc");

            // Build up the TOC from the contents.
            let html = "";
            let promise = Promise.resolve(html);

            for (let content of tocContents) {
                promise = promise
                    .then(() => this.renderTableOfContentsEntry(content))
                    .then((h) => {
                        html += h;
                        return h;
                    });
            }

            // Finish up the processing.
            resolve(
                promise.then(() => {
                    args.text = html;
                    return args;
                })
            );
        });
    }

    public renderTableOfContentsEntry(content: ContentArgs): Promise<string> {
        // Figure out what the root template is.
        let templateName: string | undefined = `toc-entry-${content.element}`;

        if (
            !Object.prototype.hasOwnProperty.call(this.templates, templateName)
        ) {
            templateName = "toc-entry";
        }

        content.logger.debug(`Rendering TOC template ${templateName}`);

        // This can potentially be a large template chain, so we start with a
        // the parameters in a hash and go from there.
        let promise: Promise<any> = Promise.resolve("");

        // Loop through and follow the extends chain until we no longer have a
        // template name to work with.
        while (templateName) {
            // Build up the render pipeline.
            let localName = templateName;

            promise = promise.then((a) =>
                this.renderTemplate(content, localName, a)
            );

            // Move to the parent item.
            const template: any = this.templates[templateName];

            if (Object.prototype.hasOwnProperty.call(template, "extends")) {
                templateName = template.extends;
            } else {
                templateName = undefined;
                break;
            }
        }

        // Return the generated TOC template.
        return promise;
    }

    public getContentTheme(_content: ContentArgs): ContentTheme {
        // Create the resulting object and return it.
        return {
            styleName: undefined,
        };
    }

    private renderTemplate(
        content: ContentArgs,
        templateName: string,
        html: string
    ): Promise<EditionArgs> {
        // Get the formatter settings.
        const settings = content.format.getSettings();

        // Set up rendering the template.
        content.logger.trace(`Rendering inner template ${templateName}`);

        const engine = new liquid.Engine();
        const templateEntry = this.templates[templateName];
        const template: string = templateEntry._contents;
        const compiled = engine.parse(template);

        // Render the template.
        let promise = compiled;
        promise = promise.then((t: any) => {
            let parameters = {
                html: html,
                content: content.metadata,
                edition: content.edition,
                theme: this,
                element: content.element,
                format: settings,
            };

            parameters.content.id = content.id;
            parameters.content.depth = content.depth;

            return t.render(parameters);
        });
        promise = promise.then((rendered: string) => {
            return rendered;
        });
        return promise;
    }

    private async renderTemplateChain(
        content: ContentArgs,
        contentTemplate: string,
        defaultTemplate: string
    ): Promise<ContentArgs> {
        // Figure out what the root template is.
        let templateName: string | undefined = contentTemplate;

        if (
            !templateName ||
            !Object.prototype.hasOwnProperty.call(this.templates, templateName)
        ) {
            templateName = defaultTemplate;
        }

        content.logger.debug(`Rendering template ${templateName}`);

        // This can potentially be a large template chain, so we start with a
        // the parameters in a hash and go from there.
        let promise: Promise<any> = Promise.resolve(content.text);

        // Loop through and follow the extends chain until we no longer have a
        // template name to work with.
        while (templateName) {
            // Build up the render pipeline.
            let localName = templateName;

            promise = promise.then((a) =>
                this.renderTemplate(content, localName, a)
            );

            // Move to the parent item.
            const template: any = this.templates[templateName];

            if (!template) {
                content.logger.error(
                    `Cannot find template named ${templateName}`
                );
            }

            if (Object.prototype.hasOwnProperty.call(template, "extends")) {
                templateName = template.extends;
            } else {
                templateName = undefined;
                break;
            }
        }

        // Update the content object and return the content.
        const html = await promise;

        content.text = html;
        content.buffer = Buffer.from(html, "utf-8");

        return content;
    }
}
