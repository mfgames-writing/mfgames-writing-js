# [@mfgames-writing/opf-v0.5.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.4.0...opf-0.5.0) (2021-07-24)

# [@mfgames-writing/opf-v0.4.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.3.0...opf-v0.4.0) (2021-07-17)

# [@mfgames-writing/opf-v0.3.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.2.12...opf-v0.3.0) (2021-07-17)

# [@mfgames-writing/opf-v0.2.12](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.2.11...opf-v0.2.12) (2021-07-17)

# [@mfgames-writing/opf-v0.2.11](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.2.10...opf-v0.2.11) (2021-07-17)

# [@mfgames-writing/opf-v0.2.10](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.2.9...opf-v0.2.10) (2021-03-15)

# [@mfgames-writing/opf-v0.2.9](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.2.8...opf-v0.2.9) (2021-03-07)

# [@mfgames-writing/opf-v0.2.8](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.2.7...opf-v0.2.8) (2021-03-06)

# [@mfgames-writing/opf-v0.2.7](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.2.6...opf-v0.2.7) (2021-03-06)

# [@mfgames-writing/opf-v0.2.6](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.2.5...opf-v0.2.6) (2021-03-06)

# [@mfgames-writing/opf-v0.2.5](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.2.4...opf-v0.2.5) (2021-03-05)

# [@mfgames-writing/opf-v0.2.4](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.2.3...opf-v0.2.4) (2021-02-04)


### Bug Fixes

* **opf:** fixed search/replace typo in `package.json` ([2f0fc8f](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/2f0fc8fecd7873dadb9609e9a74d69f4ba6b2380))

# [@mfgames-writing/opf-v0.2.3](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/opf-v0.2.2...opf-v0.2.3) (2021-02-03)


### Bug Fixes

* do not include tests in index.d.ts ([1bedf00](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/1bedf00f8dd150174e2a371ccd129d508aa0363f))

## [0.2.2](https://gitlab.com/mfgames-writing/mfgames-opf-js/compare/v0.2.1...v0.2.2) (2018-08-11)


### Bug Fixes

* added package management ([92baa46](https://gitlab.com/mfgames-writing/mfgames-opf-js/commit/92baa46))
