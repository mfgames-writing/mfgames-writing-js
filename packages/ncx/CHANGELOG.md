# [@mfgames-writing/ncx-v0.3.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.2.0...ncx-0.3.0) (2021-07-24)

# [@mfgames-writing/ncx-v0.2.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.1.0...ncx-v0.2.0) (2021-07-17)

# [@mfgames-writing/ncx-v0.1.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.0.13...ncx-v0.1.0) (2021-07-17)

# [@mfgames-writing/ncx-v0.0.13](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.0.12...ncx-v0.0.13) (2021-07-17)

# [@mfgames-writing/ncx-v0.0.12](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.0.11...ncx-v0.0.12) (2021-07-17)

# [@mfgames-writing/ncx-v0.0.11](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.0.10...ncx-v0.0.11) (2021-03-15)

# [@mfgames-writing/ncx-v0.0.10](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.0.9...ncx-v0.0.10) (2021-03-07)

# [@mfgames-writing/ncx-v0.0.9](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.0.8...ncx-v0.0.9) (2021-03-06)

# [@mfgames-writing/ncx-v0.0.8](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.0.7...ncx-v0.0.8) (2021-03-06)

# [@mfgames-writing/ncx-v0.0.7](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.0.6...ncx-v0.0.7) (2021-03-06)

# [@mfgames-writing/ncx-v0.0.6](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.0.5...ncx-v0.0.6) (2021-03-05)

# [@mfgames-writing/ncx-v0.0.5](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.0.4...ncx-v0.0.5) (2021-02-04)

# [@mfgames-writing/ncx-v0.0.4](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/ncx-v0.0.3...ncx-v0.0.4) (2021-02-03)

## [0.0.3](https://gitlab.com/mfgames-writing/mfgames-ncx-js/compare/v0.0.2...v0.0.3) (2018-08-11)


### Bug Fixes

* updating package management ([baa59b1](https://gitlab.com/mfgames-writing/mfgames-ncx-js/commit/baa59b1))
