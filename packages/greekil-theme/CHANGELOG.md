# [@mfgames-writing/greekil-theme-v1.4.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.3.0...greekil-theme-1.4.0) (2021-07-24)

# [@mfgames-writing/greekil-theme-v1.3.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.2.0...greekil-theme-v1.3.0) (2021-07-17)

# [@mfgames-writing/greekil-theme-v1.2.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.1.9...greekil-theme-v1.2.0) (2021-07-17)

# [@mfgames-writing/greekil-theme-v1.1.9](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.1.8...greekil-theme-v1.1.9) (2021-07-17)

# [@mfgames-writing/greekil-theme-v1.1.8](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.1.7...greekil-theme-v1.1.8) (2021-07-17)

# [@mfgames-writing/greekil-theme-v1.1.7](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.1.6...greekil-theme-v1.1.7) (2021-03-15)

# [@mfgames-writing/greekil-theme-v1.1.6](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.1.5...greekil-theme-v1.1.6) (2021-03-07)

# [@mfgames-writing/greekil-theme-v1.1.5](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.1.4...greekil-theme-v1.1.5) (2021-03-06)


### Bug Fixes

* **greekil-theme:** always extract assets during build ([e022267](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/e0222675025a56092c8668fa4161217b8dc18a08))

# [@mfgames-writing/greekil-theme-v1.1.4](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.1.3...greekil-theme-v1.1.4) (2021-03-06)


### Bug Fixes

* **greekil-theme:** trying to package font ([ebedd61](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/ebedd6156a38e35e8b2c7bd799be408be0f716f0))

# [@mfgames-writing/greekil-theme-v1.1.3](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.1.2...greekil-theme-v1.1.3) (2021-03-06)


### Bug Fixes

* **greekil-theme:** package assets that moved with lerna ([84d23af](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/84d23af43a8528e2e559f4c673ff8de87e6d7d4a))

# [@mfgames-writing/greekil-theme-v1.1.2](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.1.1...greekil-theme-v1.1.2) (2021-03-05)

# [@mfgames-writing/greekil-theme-v1.1.1](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.1.0...greekil-theme-v1.1.1) (2021-02-04)

# [@mfgames-writing/greekil-theme-v1.1.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/greekil-theme-v1.0.8...greekil-theme-v1.1.0) (2021-02-03)


### Bug Fixes

* correcting mistakes from monorepo merging and refactoring ([7d44c63](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/7d44c632a51a05a69bb1cd8dbb81a8b603a6d0d1))

# [@mfgames-writing/greekil-theme-v1.0.8](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/greekil-theme-v1.0.7...greekil-theme-v1.0.8) (2020-07-26)
