# [@mfgames-writing/contracts-v3.5.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.4.0...contracts-3.5.0) (2021-07-24)

# [@mfgames-writing/contracts-v3.4.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.3.0...contracts-v3.4.0) (2021-07-17)

# [@mfgames-writing/contracts-v3.3.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.2.10...contracts-v3.3.0) (2021-07-17)

# [@mfgames-writing/contracts-v3.2.10](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.2.9...contracts-v3.2.10) (2021-07-17)

# [@mfgames-writing/contracts-v3.2.9](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.2.8...contracts-v3.2.9) (2021-07-17)

# [@mfgames-writing/contracts-v3.2.8](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.2.7...contracts-v3.2.8) (2021-03-15)

# [@mfgames-writing/contracts-v3.2.7](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.2.6...contracts-v3.2.7) (2021-03-07)

# [@mfgames-writing/contracts-v3.2.6](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.2.5...contracts-v3.2.6) (2021-03-06)

# [@mfgames-writing/contracts-v3.2.5](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.2.4...contracts-v3.2.5) (2021-03-06)

# [@mfgames-writing/contracts-v3.2.4](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.2.3...contracts-v3.2.4) (2021-03-06)

# [@mfgames-writing/contracts-v3.2.3](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.2.2...contracts-v3.2.3) (2021-03-05)

# [@mfgames-writing/contracts-v3.2.2](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.2.1...contracts-v3.2.2) (2021-02-04)

# [@mfgames-writing/contracts-v3.2.1](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/contracts-v3.2.0...contracts-v3.2.1) (2021-02-03)

# [@mfgames-writing/contracts-v3.2.0](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/contracts-v3.1.13...contracts-v3.2.0) (2020-07-26)

# [@mfgames-writing/contracts-v3.1.13](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/contracts-v3.1.12...contracts-v3.1.13) (2020-07-26)

# [@mfgames-writing/contracts-v3.1.6](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/contracts-v3.1.5...contracts-v3.1.6) (2020-07-26)

## [3.1.5](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/compare/v3.1.4...v3.1.5) (2018-08-12)


### Bug Fixes

* moved dev deps out of deps ([64614f6](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/commit/64614f6))

## [3.1.4](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/compare/v3.1.3...v3.1.4) (2018-08-09)


### Bug Fixes

* fixing tsconfig.json to put files in right place ([f16365b](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/commit/f16365b))

## [3.1.3](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/compare/v3.1.2...v3.1.3) (2018-08-07)


### Bug Fixes

* fixing main inside package.json ([e82405a](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/commit/e82405a))

## [3.1.2](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/compare/v3.1.1...v3.1.2) (2018-08-07)


### Bug Fixes

* adding NPM publish to semantic release ([7d3901a](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/commit/7d3901a))

## [3.1.1](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/compare/v3.1.0...v3.1.1) (2018-08-07)


### Bug Fixes

* organizing code base ([70ac808](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/commit/70ac808))
