export class LoggerWrapper {
    constructor(public name: string, public logger: any) {
        this.logger = logger;
        this.name = name;
    }

    public trace(v0: any, ...v1: any[]) {
        this.logger.trace(`${this.name}: ${v0}`, ...v1);
    }
    public debug(v0: any, ...v1: any[]) {
        this.logger.debug(`${this.name}: ${v0}`, ...v1);
    }
    public info(v0: any, ...v1: any[]) {
        this.logger.info(`${this.name}: ${v0}`, ...v1);
    }
    public warn(v0: any, ...v1: any[]) {
        this.logger.warn(`${this.name}: ${v0}`, ...v1);
    }
    public error(v0: any, ...v1: any[]) {
        this.logger.error(`${this.name}: ${v0}`, ...v1);
    }
}
