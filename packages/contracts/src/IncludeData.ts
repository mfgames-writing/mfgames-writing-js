/**
 * Information about a block of data that needs to be injected into the
 * publication data, such as to include additional information from an
 * external file or to share settings across multiple points.
 */
export interface IncludeData {
    /**
     * The path of a JSON file to be inserted into the publication file.
     * Relative paths are relative to the same directory as the publication
     * file.
     */
    json: string;

    /**
     * The "path" inside the publication file to merge the results. This can
     * either be a single path, such as `edition.pdf`, or a list of paths
     * such as `[edition.pdf, edition.epub]`.
     */
    merge: string | string[];
}
