/**
 * Contains additional information about a book series.
 */
export interface MetadataSeriesData {
    name: string;
    volume: number | undefined;
}
