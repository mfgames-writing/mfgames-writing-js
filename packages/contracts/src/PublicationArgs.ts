import { PublicationData } from "./PublicationData";

export class PublicationArgs {
    public publication: PublicationData;
    public rootDirectory: string;
    public publicationFile: string;
    public package: any;

    constructor(public logger: any, public project: any, public argv: any) {
        this.package = { ...this.project };
    }
}
