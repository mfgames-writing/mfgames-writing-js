import { ContentData } from "./ContentData";
import { IncludeData } from "./IncludeData";
import { MetadataData } from "./MetadataData";

/**
 * Interface that describes the contents of the publish file regardless of the
 * file format.
 */
export interface PublicationData {
    /** Metdata associated with the project. */
    metadata: MetadataData;

    /**
     * Information about the editions (build targets). This is used to
     * identify different types of output (PDF, EPUB) or variants such as
     * a Kickstarter-centric or a default one.
     */
    editions: any;

    /**
     * An optional list of include that can pull in data from another
     * source.
     */
    includes?: IncludeData[];

    /**
     * The individual components to the project, such as chapters or
     * front/back matter, titles, etc.
     */
    contents: ContentData[];

    /**
     * A dynamically inserted path that indicates the root of the project.
     */
    rootDirectory?: string;

    /**
     * A dynamically inserted path to the publication file.
     */
    publicationFile?: string;
}
