import { Options } from "markdown-it";
import { EditionMarkdownExtensionData } from "./EditionMarkdownExtensionData";

/**
 * Configures various settings for parsing Markdown using `markdown-it`.
 */
export interface EditionMarkdownData {
    /**
     * Additional options for the Markdown.
     */
    options?: Options;

    /**
     * Additional extensions, if needed.
     */
    extensions?: EditionMarkdownExtensionData[];
}
