import { ContentArgs } from "./ContentArgs";
import { EditionData } from "./EditionData";
import { Formatter } from "./formatters";
import { LoggerWrapper } from "./LoggerWrapper";
import { PublicationArgs } from "./PublicationArgs";
import { Theme } from "./Theme";

export class EditionArgs extends PublicationArgs {
    public name: string;
    public edition: EditionData;
    public theme: Theme;
    public format: Formatter;
    public contents: ContentArgs[] = [];

    constructor(args: PublicationArgs, name: string, edition: EditionData) {
        super(new LoggerWrapper(name, args.logger), args.project, args.argv);
        this.publication = args.publication;
        this.name = name;
        this.edition = edition;
        this.rootDirectory = args.rootDirectory;
        this.package = args.package;
    }
}
