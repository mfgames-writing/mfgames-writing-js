import { ContentArgs } from "./ContentArgs";

/**
 * Defines the public signature for a pipeline operator which processes
 * individual contents and returns a promise to manipulate them.
 */
export interface ContentPipeline {
    process(content: ContentArgs, args: any): Promise<ContentArgs>;
}
