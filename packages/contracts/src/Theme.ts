import { ContentArgs } from "./ContentArgs";
import { ContentRenderCallback } from "./ContentRenderCallback";
import { ContentTheme } from "./ContentTheme";
import { EditionArgs } from "./EditionArgs";
import { PandocThemeSettings } from "./PandocThemeSettings";

export interface Theme {
    /**
     * Optional settings for Pandoc-specific settings for formatters that use
     * that program for formatting.
     */
    pandocSettings?: PandocThemeSettings;

    /** The name of the stylesheet as included in the generated HTML files. */
    stylesheetFileName: string;

    /**
     * An optional method to override the opening tag for blockquotes.
     */
    renderBlockQuoteOpen?: ContentRenderCallback;

    /**
     * An optional method to override the closing tag for blockquotes.
     */
    renderBlockQuoteClose?: ContentRenderCallback;

    /**
     * An optional method to override rendering of rules (breaks).
     */
    renderRule?: ContentRenderCallback;

    /**
     * An optional method to override rendering of opening strong (bold) tag.
     */
    renderStrongOpen?: ContentRenderCallback;

    /**
     * An optional method to override rendering of closing strong (bold) tag.
     */
    renderStrongClose?: ContentRenderCallback;

    /**
     * An optional method to override rendering of opening emphasis (i) tag.
     */
    renderEmphasisOpen?: ContentRenderCallback;

    /**
     * An optional method to override rendering of closing emphasis (i) tag.
     */
    renderEmphasisClose?: ContentRenderCallback;

    /**
     * An optional method to override rendering inline code.
     */
    renderCodeSpan?: ContentRenderCallback;

    start(args: EditionArgs): Promise<EditionArgs>;
    finish(args: EditionArgs): Promise<EditionArgs>;
    renderHtml(args: ContentArgs): Promise<ContentArgs>;

    /**
     * Renders the HTML chrome and layout elements such as the html, head, and
     * body elements around the contents given by the contents.
     */
    renderLayout(args: ContentArgs): Promise<ContentArgs>;

    renderNavigationTitle(args: ContentArgs): string;

    /**
     * Renders the stylesheet with various substitions for the edition with an
     * optional mode to use for specifics.
     *
     * @param {EditionArgs} args The edition being processed.
     * @param {string|string[]} mode An optional mode list which will select a
     *   specific stylesheet if available.
     */
    renderStylesheet(args: EditionArgs, mode?: string | string[]): Buffer;

    renderTableOfContents(args: ContentArgs): Promise<ContentArgs>;

    /**
     * Retrieves information about a specific element, such as how it is will be
     * rendered with the resulting stylesheet.
     *
     * @param {ContentArgs} content The content currently being processed.
     */
    getContentTheme(content: ContentArgs): ContentTheme;

    /**
     * Transform the rendered HTML in some manner.
     */
    transformHtml?(content: ContentArgs, html: string): string;
}
