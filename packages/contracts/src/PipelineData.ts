/**
 * Describes the common properties of a pipeline configuration. The rest of the
 * object is passed "as-is" as parameters for the plugin.
 */
export interface PipelineData {
    module: string;
}
