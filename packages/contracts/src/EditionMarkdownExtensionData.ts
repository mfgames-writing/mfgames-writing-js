/**
 * Indicates that a `markdown-it` extension that needs to be loaded.
 */
export interface EditionMarkdownExtensionData {
    /**
     * The name of the NPM package to get the extension.
     */
    package: string;

    /**
     * The optional settings for the package.
     */
    options?: any;

    /**
     * An optional script, loaded as a module with a default export, that is
     * used to populate or expand the options. This takes the options from
     * this class or `{}` if none are given.
     */
    script: string;
}
