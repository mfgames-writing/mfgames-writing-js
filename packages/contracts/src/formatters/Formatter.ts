import { ContentArgs } from "../ContentArgs";
import { EditionArgs } from "../EditionArgs";
import { FormatImageRequest } from "./FormatImageRequest";
import { FormatImageResponse } from "./FormatImageResponse";
import { FormatterSettings } from "./FormatterSettings";

/**
 * Defines the public interface for a formatter which processes individual
 * files.
 */
export interface Formatter {
    getSettings(): FormatterSettings;
    start(edition: EditionArgs): Promise<EditionArgs>;
    finish(edition: EditionArgs): Promise<EditionArgs>;
    addHtml(content: ContentArgs): Promise<ContentArgs>;
    addImage(
        content: ContentArgs,
        image: FormatImageRequest
    ): FormatImageResponse;
}
