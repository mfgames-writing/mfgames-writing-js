import { ContentArgs } from "../ContentArgs";
import { FormatImageRequest } from "./FormatImageRequest";

export interface FormatImageCallback {
    (request: FormatImageRequest): Promise<ContentArgs>;
}
