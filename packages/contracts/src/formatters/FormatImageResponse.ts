/**
 * Defines the response from the formatter about an image.
 */
export interface FormatImageResponse {
    include: boolean;
    href?: string;
    callback?: any;
}
