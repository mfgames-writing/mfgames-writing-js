import { ContentArgs } from "./ContentArgs";
import Token = require("markdown-it/lib/token");
import MarkdownIt = require("markdown-it");
import Renderer = require("markdown-it/lib/renderer");

export type ContentRenderCallback = (
    args: ContentArgs,
    tokens: Token[],
    idx: number,
    options: MarkdownIt.Options,
    env: any,
    renderer: Renderer
) => string;
