export interface PandocThemeSettings {
    /** The name of the template file for .docx files. */
    docxTemplatePath?: string;

    /** The name of the template file for .odt files. */
    odtTemplatePath?: string;

    /** The absolute path to the data directory. */
    dataDirectory?: string;
}
