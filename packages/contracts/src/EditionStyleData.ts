import { Options } from "markdown-it";
import { EditionMarkdownExtensionData } from "./EditionMarkdownExtensionData";

/**
 * Configures various settings for additional style data.
 */
export interface EditionStyleData {
    /**
     * Additional CSS that will be appended to the stylesheet. This
     * will not go through the style lookup functions.
     */
    css?: string;
}
