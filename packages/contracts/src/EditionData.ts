import { EditionImagesData } from "./EditionImagesData";
import { MetadataData } from "./MetadataData";
import { EditionMarkdownData } from "./EditionMarkdownData";
import { EditionStyleData } from "./EditionStyleData";

/**
 * Contains information about the different editions or variants of the
 * publication. This is contained in the `editions` element of the main file,
 * but TypeScript cannot define that.
 */
export interface EditionData extends MetadataData {
    outputDirectory?: string;
    outputFilename?: string;
    extends?: string;
    format: string;
    theme: string;
    images?: EditionImagesData;
    markdown?: EditionMarkdownData;
    rootDirectory?: string;
    publicationFile?: string;
    style?: EditionStyleData;
}
