import {
    ContentArgs,
    ContentData,
    EditionArgs,
    FormatImageRequest,
    FormatImageResponse,
    Formatter,
    FormatterSettings,
} from "@mfgames-writing/contracts";
import * as child from "child_process";
import * as tmp from "tmp";
import * as fs from "fs";
import * as path from "path";
import * as zpad from "zpad";

export class DocxFormatter implements Formatter {
    private buffer: string = "";
    private stylesheet: string;
    private lastContent: ContentArgs;
    private contentCounter = 0;
    private workDirectory: string;
    private createdWorkDirectory: boolean;

    public getSettings(): FormatterSettings {
        var settings = new FormatterSettings();
        settings.wrapIndividualFiles = false;
        return settings;
    }

    public start(args: EditionArgs): Promise<EditionArgs> {
        // If the user provides --temp on the CLI, then use that.
        this.workDirectory = args.argv.temp;

        // If we haven't had a work directory provided, make up one.
        if (!this.workDirectory) {
            this.createdWorkDirectory = true;
            this.workDirectory = tmp.dirSync().name;
        }

        args.logger.debug(`Using temporary directory: ${this.workDirectory}`);

        // Start with a simple promise because it makes it easier to maintain.
        let promise = Promise.resolve(args);

        // We want to grab all of the stylesheet since we are going to replace
        // it inline in the resulting HTML.
        promise = promise.then((a) => {
            this.stylesheet = args.theme
                .renderStylesheet(args, ["html"])
                .toString();
            return args;
        });

        // Return the resulting promise.
        return promise;
    }

    public addHtml(content: ContentArgs): Promise<ContentArgs> {
        const isFirst = !this.lastContent;

        this.lastContent = content;

        return Promise.resolve(content).then((c) => {
            // Insert a page break if we aren't the first. This uses the
            // `pagebreak.lua` filter that we also package in our library and
            // catches on the form-feed (`\f`) character in a paragraph by
            // itself.
            if (!isFirst) {
                this.buffer += "\n<p class='page-break'>\f</p>\n";
            }

            // Unlike most of the formatters, we cannot handle hyphenation so
            // we manually remove them from the input.
            this.buffer += c.text.replace(/&#173;/g, "");
            return c;
        });
    }

    public addImage(
        content: ContentArgs,
        image: FormatImageRequest
    ): FormatImageResponse {
        // If we don't have an image, skip it.
        if (!image.buffer) {
            content.logger.debug(`Missing image: ${image.imagePath}`);
            throw new Error();
        }

        // Convert the image into base 64 to we can embed it directly.
        const encoded = image.buffer.toString("base64");
        const href = "data:image/png;base64," + encoded;

        // Return the resulting data.
        return {
            include: true,
            href: href,
            callback: () => content,
        };
    }

    public finish(args: EditionArgs): Promise<EditionArgs> {
        // Add in the various final steps we need. We start with a simple
        // promise and then chain it so we can easily maintain this logic.
        let promise: Promise<any> = Promise.resolve(args);

        // First we need to wrap the contents of the entire HTML in chrome.
        // With the way this is written, we have to fake the ContentArgs to
        // represent the entire buffer as a single object.
        promise = promise.then((a) => {
            var contentData = {
                id: `h${zpad(this.contentCounter++, 4)}`,
                element: this.lastContent.metadata.element,
                directory: this.lastContent.metadata.directory,
                source: this.lastContent.metadata.source,
                depth: this.lastContent.metadata.depth,
                text: "",
            } as ContentData;
            var contentArgs = new ContentArgs(args, contentData);
            contentArgs.text = this.buffer;

            return contentArgs;
        });
        promise = promise.then((c) => args.theme.renderLayout(c));

        // Write out the resulting HTML file to a temporary location.
        promise = promise.then((c) => {
            // Figure out the filename we'll be writing.
            let htmlFilename = path.join(
                this.workDirectory,
                args.edition.outputDirectory ?? ".",
                args.edition.outputFilename + ".html"
            );

            args.logger.info(`Writing out HTML: ${htmlFilename}`);

            // Write out the file and return the results. While we do that,
            // we also inline the entire stylesheet so we can have a single,
            // self-contained HTML file.
            fs.writeFileSync(
                htmlFilename,
                c.text.replace(/<link.*?>/, `<style>${this.stylesheet}</style>`)
            );

            // Figure out the Pandoc arguments.
            const outputFilename = args.edition.outputFilename;

            if (!outputFilename) {
                throw new Error("Edition does not have an output file name");
            }

            const docFilename = path.join(
                args.rootDirectory,
                args.edition.outputDirectory ?? ".",
                outputFilename
            );
            const filterFilename = path.join(__dirname, "..", "pagebreak.lua");
            let execArgs = [
                htmlFilename,
                "-o",
                docFilename,
                "-t",
                "docx",
                "--lua-filter=" + filterFilename,
            ];

            // If the theme has a reference template, we use that.
            const pandocSettings = args.theme.pandocSettings;

            if (pandocSettings) {
                if (pandocSettings.dataDirectory) {
                    execArgs.push("--data-dir=" + pandocSettings.dataDirectory);
                }

                if (pandocSettings.docxTemplatePath) {
                    execArgs.push(
                        "--reference-doc=" + pandocSettings.docxTemplatePath
                    );
                }
            }

            // Run Pandoc
            args.logger.debug(`pandoc ${execArgs.join(" ")}`);
            args.logger.info(`Writing out Word Doc: ${docFilename}`);

            child.execFileSync("pandoc", execArgs, {
                cwd: this.workDirectory,
            });

            return args;
        });

        // Return the resulting promise.
        return promise;
    }
}
