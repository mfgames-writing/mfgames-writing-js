import { DocxFormatter } from "./DocxFormatter";

export * from "./index";

export default function loadDocxFormatter() {
    return new DocxFormatter();
}
