# [@mfgames-writing/guillemet-pipeline-v1.4.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.3.0...guillemet-pipeline-1.4.0) (2021-07-24)

# [@mfgames-writing/guillemet-pipeline-v1.3.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.2.0...guillemet-pipeline-v1.3.0) (2021-07-17)

# [@mfgames-writing/guillemet-pipeline-v1.2.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.1.9...guillemet-pipeline-v1.2.0) (2021-07-17)

# [@mfgames-writing/guillemet-pipeline-v1.1.9](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.1.8...guillemet-pipeline-v1.1.9) (2021-07-17)

# [@mfgames-writing/guillemet-pipeline-v1.1.8](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.1.7...guillemet-pipeline-v1.1.8) (2021-07-17)

# [@mfgames-writing/guillemet-pipeline-v1.1.7](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.1.6...guillemet-pipeline-v1.1.7) (2021-03-15)

# [@mfgames-writing/guillemet-pipeline-v1.1.6](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.1.5...guillemet-pipeline-v1.1.6) (2021-03-07)

# [@mfgames-writing/guillemet-pipeline-v1.1.5](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.1.4...guillemet-pipeline-v1.1.5) (2021-03-06)

# [@mfgames-writing/guillemet-pipeline-v1.1.4](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.1.3...guillemet-pipeline-v1.1.4) (2021-03-06)

# [@mfgames-writing/guillemet-pipeline-v1.1.3](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.1.2...guillemet-pipeline-v1.1.3) (2021-03-06)

# [@mfgames-writing/guillemet-pipeline-v1.1.2](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.1.1...guillemet-pipeline-v1.1.2) (2021-03-05)

# [@mfgames-writing/guillemet-pipeline-v1.1.1](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.1.0...guillemet-pipeline-v1.1.1) (2021-02-04)

# [@mfgames-writing/guillemet-pipeline-v1.1.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/guillemet-pipeline-v1.0.1...guillemet-pipeline-v1.1.0) (2021-02-03)


### Bug Fixes

* correcting mistakes from monorepo merging and refactoring ([7d44c63](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/7d44c632a51a05a69bb1cd8dbb81a8b603a6d0d1))

# [@mfgames-writing/guillemet-pipeline-v1.0.1](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/guillemet-pipeline-v1.0.0...guillemet-pipeline-v1.0.1) (2020-07-26)

# 1.0.0 (2018-08-10)


### Bug Fixes

* adding package management ([8c0b026](https://gitlab.com/mfgames-writing/mfgames-writing-guillemet-js/commit/8c0b026))
