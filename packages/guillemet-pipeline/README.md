# Guilllemet plugin support for MfGames Writing (Javascript).

This is a pipeline module which converts `<<` and `>>` into guillemets (`«` and `»` respectively). This is used for those who use those forms of quotation in their writing.

This plugin is used in the `pipeline` elements inside a `publication.yaml` file.

```yaml
contents:
  - element: chapter
    number: 1
    directory: chapters
    source: /^chapter-\d+.markdown$/
    start: true
    page: 1
    pipeline: &pipelines
      - module: fedran-hyphen
```
