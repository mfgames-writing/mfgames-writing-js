import { ContentArgs, ContentPipeline } from "@mfgames-writing/contracts";

export function loadGuillemetPlugin(args: any): GuillemetPlugin {
    return new GuillemetPlugin();
}

export class GuillemetPlugin implements ContentPipeline {
    public process(content: ContentArgs): Promise<ContentArgs> {
        return new Promise<ContentArgs>((resolve, reject) => {
            content.text = content.text.replace(/<</g, "«").replace(/>>/g, "»");

            resolve(content);
        });
    }
}
