# [@mfgames-writing/epub2-v1.5.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.4.0...epub2-1.5.0) (2021-07-24)

# [@mfgames-writing/epub2-v1.4.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.3.0...epub2-v1.4.0) (2021-07-17)


### Features

* **epub2:** added the ability to include assets ([83e77a5](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/83e77a5772ca877ff80241c18d2e578e25c998c3))

# [@mfgames-writing/epub2-v1.3.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.2.9...epub2-v1.3.0) (2021-07-17)


### Features

* **epub2:** only include images once by using md5 hash ([fae6544](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/fae654418f8a3c09a9e868de618694913a354d3c))

# [@mfgames-writing/epub2-v1.2.9](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.2.8...epub2-v1.2.9) (2021-07-17)

# [@mfgames-writing/epub2-v1.2.8](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.2.7...epub2-v1.2.8) (2021-07-17)

# [@mfgames-writing/epub2-v1.2.7](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.2.6...epub2-v1.2.7) (2021-03-15)

# [@mfgames-writing/epub2-v1.2.6](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.2.5...epub2-v1.2.6) (2021-03-07)

# [@mfgames-writing/epub2-v1.2.5](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.2.4...epub2-v1.2.5) (2021-03-06)

# [@mfgames-writing/epub2-v1.2.4](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.2.3...epub2-v1.2.4) (2021-03-06)

# [@mfgames-writing/epub2-v1.2.3](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.2.2...epub2-v1.2.3) (2021-03-06)

# [@mfgames-writing/epub2-v1.2.2](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.2.1...epub2-v1.2.2) (2021-03-05)

# [@mfgames-writing/epub2-v1.2.1](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.2.0...epub2-v1.2.1) (2021-02-04)

# [@mfgames-writing/epub2-v1.2.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/epub2-v1.1.10...epub2-v1.2.0) (2021-02-03)


### Bug Fixes

* correcting mistakes from monorepo merging and refactoring ([7d44c63](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/7d44c632a51a05a69bb1cd8dbb81a8b603a6d0d1))

# [@mfgames-writing/epub2-v1.1.10](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/epub2-v1.1.9...epub2-v1.1.10) (2020-07-26)

# [@mfgames-writing/epub2-v1.1.3](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/epub2-v1.1.2...epub2-v1.1.3) (2020-07-26)

## [1.1.2](https://gitlab.com/mfgames-writing/mfgames-writing-epub-js/compare/v1.1.1...v1.1.2) (2018-08-11)


### Bug Fixes

* adding package mangagement ([a6cef52](https://gitlab.com/mfgames-writing/mfgames-writing-epub-js/commit/a6cef52))
