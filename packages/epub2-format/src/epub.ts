import {
    ContentArgs,
    EditionArgs,
    FormatImageRequest,
    FormatImageResponse,
    Formatter,
    FormatterSettings,
} from "@mfgames-writing/contracts";
import * as ncx from "@mfgames-writing/ncx";
import * as opf from "@mfgames-writing/opf";
import * as crypto from "crypto";
import * as fs from "fs";
import * as path from "path";
import { addOpfMetadata } from "./EpubOpf";
import JSZip = require("jszip");
import zpad = require("zpad");
import { EpubAssetData } from "./EpubAssetData";
import { EpubContentData } from "./EpubContentData";
import * as htmlparser2 from "htmlparser2";

export function loadEpubFormatter() {
    return new EpubFormatter();
}

export class EpubFormatter implements Formatter {
    private zip: any;
    private ncx: ncx.Ncx;
    private opf: opf.Opf;
    private existingZipIds: string[] = [];
    private contentCounter = 0;

    public getSettings(): FormatterSettings {
        var settings = new FormatterSettings();
        settings.wrapIndividualFiles = true;
        return settings;
    }

    public start(args: EditionArgs): Promise<EditionArgs> {
        // Create the OPF container.
        this.opf = new opf.Opf();
        addOpfMetadata(args, this.opf);

        // Create the NCX identifier.
        this.ncx = new ncx.Ncx();
        this.ncx.title = args.publication.metadata.title ?? args.edition.title;
        this.ncx.uid = this.opf.uniqueIdentifier.id;

        // Start with a simple promise that makes it
        // easier to format our code.
        let promise = Promise.resolve(args);

        // Create the zip archive we'll be creating as the EPUB.
        promise = promise.then((v) => {
            this.zip = new JSZip();
            return args;
        });

        // The `mimetype` must be the first included in the archive.
        promise = promise.then((v) => this.addMimeType(args));

        // Return the resulting promise.
        return promise;
    }

    public addHtml(content: ContentArgs): Promise<ContentArgs> {
        return Promise.resolve(content).then((a) => this.addHtmlZip(a));
    }

    public addImage(
        content: ContentArgs,
        image: FormatImageRequest
    ): FormatImageResponse {
        // Get the hash of the file which will let us produce smaller EPUB
        // archives by only including a file once.
        // change to 'md5' if you want an MD5 hash
        let id = this.getFileId(image.imagePath);

        // Figure out the name of the file in the zip archive.
        let zipFileName = `Images/${id}${image.extension}`;

        if (content.element === "cover") {
            // We have set names, ids, and scaling for covers.
            id = "cover-image";
            zipFileName = `cover-image${image.extension}`;
        }

        // We only want to add a single version to the zip archive. If we return
        // null from this function, then we already have it.
        if (this.existingZipIds.filter((i) => i === id).length > 0) {
            return {
                include: false,
                href: zipFileName,
            };
        }

        this.existingZipIds.push(id);

        // We have to add this image to the manifest. We need to include the
        // correct type.
        this.opf.manifests.push(
            new opf.OpfManifest(id, zipFileName, image.mime)
        );

        // Create the promise to actually add it.
        let callback = (img: FormatImageRequest) => {
            return new Promise((resolve, reject) => {
                // Add it to the zip archive.
                this.zip.file(zipFileName, img.buffer, {
                    compression: "DEFLATE",
                });
                content.logger.debug(`Added image ${id}`);
                resolve(content);
            });
        };

        // Return the resulting data.
        return {
            include: true,
            href: zipFileName,
            callback: callback,
        };
    }

    public finish(args: EditionArgs): Promise<EditionArgs> {
        // Add in the various final steps we need.
        let promise: Promise<EditionArgs> = Promise.resolve(args);
        promise = promise.then(() => this.addStylesheet(args));
        promise = promise.then(() => this.addAssets(args));
        promise = promise.then(() => this.addTocNcx(args));
        promise = promise.then(() => this.addContentOpf(args));
        promise = promise.then(() => this.addContainerXml(args));
        promise = promise.then(() => this.addAppleOptions(args));

        // Write out the zip archive as an EPUB.
        promise = promise.then(() => this.writeEpub(args));

        // Return the resulting promise.
        return promise;
    }

    private addHtmlZip(content: ContentArgs): Promise<ContentArgs> {
        return new Promise<ContentArgs>((resolve, reject) => {
            // We have to normalize some of the HTML because `epubcheck` doesn't
            // like when HTML tables are spanned across multiple lines, it wants
            // them as a single line.
            const html = content.buffer
                .toString()
                .replace(/<(\/?(tr|td|th|thead|tbody|table))>\n/g, "<$1>")
                .replace(/<tr><\/tr>/g, "");

            // Figure out which index we'll be using and use that for the
            // filename. Content can't have a leading number, so we add a
            // constant that lets us sort by the play order.
            let element = content.element;
            let id = content.id;
            let filename = `${id}.xhtml`;
            content.logger.debug(`Adding ${filename}`);

            // Add this as a buffer to the zip archive.
            this.zip.file(filename, html, { compression: "DEFLATE" });

            // Add this item to the manifest and spine.
            const spine = new opf.OpfSpine(id);

            this.opf.spines.push(spine);
            this.opf.manifests.push(
                new opf.OpfManifest(id, filename, "application/xhtml+xml")
            );

            if (content.linear !== undefined) {
                spine.linear = content.linear;
            }

            // Based on the element type, we'll have additional processes.
            if (content.contentData.start) {
                content.logger.info(`Setting start to ${id}`);
                this.opf.guides.text = new opf.OpfGuide(
                    content.metadata.title,
                    filename
                );
            }

            switch (element) {
            case "cover":
                this.opf.guides.cover = new opf.OpfGuide(
                    content.metadata.title,
                    filename
                );
                break;
            case "dedication":
                this.opf.guides.dedication = new opf.OpfGuide(
                    content.metadata.title,
                    filename
                );
                break;
            case "colophon":
                this.opf.guides.colophon = new opf.OpfGuide(
                    content.metadata.title,
                    filename
                );
                break;
            case "toc":
                this.opf.guides.toc = new opf.OpfGuide(
                    content.metadata.title,
                    filename
                );
                break;
            case "acknowledgement":
                this.opf.guides.acknowledgements = new opf.OpfGuide(
                    content.metadata.title,
                    filename
                );
                break;
            }

            // Add the navigation point, which may be recursive.
            this.addHtmlNcx(content, id, filename);

            // Finish up this promise.
            resolve(content);
        });
    }

    private addHtmlNcx(
        content: ContentArgs,
        id: string,
        filename: string
    ): void {
        // Add the top-level navigation point.
        const epubContentData = content.contentData as EpubContentData;
        const navigationTitle = content.theme.renderNavigationTitle(content);
        const navigationPoint = this.ncx.addPoint(
            id,
            navigationTitle,
            filename,
            content.parent ? content.parent.process["ncx"] : undefined
        );

        content.process["ncx"] = navigationPoint;

        // If we aren't including headers, then we're done.
        const headers = epubContentData.includeHeadersInNcx;

        if (!epubContentData.includeHeadersInNcx) {
            return;
        }

        // While we are parsing, we keep track of the "depth" of the items by
        // using normal conventions of nested headers where "H0" is the file,
        // H1 is index 1, H2 is index 2. As we encountered a header, we roll
        // back until the appropriate level to figure out the parent.
        let parentPoints = [navigationPoint];

        // Parse through the content and get the h2 and later points.
        const ncx = this.ncx;
        let inHeader = false;
        let text = "";
        let headerId = "";
        let headerCount = 0;
        const parser = new htmlparser2.Parser({
            onopentag(name: string, attributes: any) {
                const match = name.match(/^h(\d+)$/);

                // If the identifier is in an anchor inside the header, then
                // we have to pick it up.
                if (
                    !match &&
                    headerId === undefined &&
                    inHeader &&
                    attributes.id
                ) {
                    headerId = attributes.id;
                }

                // If we have a match on header and it is in the list of headers
                // then include it.
                if (match && !!headers[name]) {
                    text = "";
                    inHeader = true;
                    headerId = attributes.id;
                }
            },
            ontext(data: string) {
                if (inHeader) {
                    text += data;
                }
            },
            onclosetag(name: string) {
                // If we aren't in a header, we don't care.
                // We only care about headers.
                const match = name.match(/^h(\d+)$/);

                if (!match || !headers[name]) {
                    return;
                }

                // We aren't in a header now.
                inHeader = false;

                // Roll back the previous depth to the one above this.
                const level = parseInt(match[1]);

                while (parentPoints.length >= level) {
                    parentPoints.pop();
                }

                // Get the parent item, there should always be one of them. Now
                // this logic isn't *entirely* accurate since it will add a
                // child point to a parent regardless of the level. This means
                // if there is a H1 followed by a H3, it will just treat the H3
                // as a H2.
                const parentPoint = parentPoints[parentPoints.length - 1];
                const childPoint = ncx.addPoint(
                    `${id}-${headerCount++}`,
                    text,
                    filename + "#" + headerId,
                    parentPoint
                );

                parentPoints.push(childPoint);
            },
        });

        parser.write(content.buffer.toString());
        parser.end();
    }

    private addAssets(args: EditionArgs): Promise<EditionArgs> {
        return new Promise<EditionArgs>((resolve) => {
            // If we don't have any included elements, then don't add them.
            const data = args.edition as any;

            if (!data.assets) {
                args.logger.debug("No edition.assets, skipping adding assets");
                return resolve(args);
            }

            // Pull out the assets as an array of data.
            const assets: EpubAssetData[] = data.assets;

            for (const asset of assets) {
                const localPath = asset.path;
                const archivePath = asset.archivePath ?? localPath;
                const id = this.getFileId(asset.path);
                const mime = asset.mime ?? "application/octet-stream";

                args.logger.debug(
                    `Adding asset ${id} from ${localPath} to ${archivePath} (${mime})`
                );

                // Add it to the OPF manifest.
                this.opf.manifests.push(
                    new opf.OpfManifest(id, archivePath, mime)
                );

                // Add it to the zip archive.
                const buffer = fs.readFileSync(localPath);

                this.zip.file(archivePath, buffer, { compression: "DEFLATE" });
            }

            // Finish up processing the file.
            resolve(args);
        });
    }

    private addAppleOptions(args: EditionArgs): Promise<EditionArgs> {
        return new Promise<EditionArgs>((resolve) => {
            this.zip.file(
                "META-INF/com.apple.ibooks.display-options.xml",
                Buffer.from(
                    `<?xml version="1.0" encoding="UTF-8"?>
        <display_options>
          <platform name="*">
            <option name="specified-fonts">true</option>
          </platform>
        </display_options>`,
                    "utf8"
                ),
                { compression: "DEFLATE" }
            );
            resolve(args);
        });
    }

    private addContainerXml(args: EditionArgs): Promise<EditionArgs> {
        return new Promise<EditionArgs>((resolve, reject) => {
            this.zip.file(
                "META-INF/container.xml",
                Buffer.from(
                    `<?xml version="1.0" encoding="UTF-8"?>
        <container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
          <rootfiles>
            <rootfile full-path="content.opf" media-type="application/oebps-package+xml" />
          </rootfiles>
        </container>`,
                    "utf8"
                ),
                { compression: "DEFLATE" }
            );
            resolve(args);
        });
    }

    private addContentOpf(args: EditionArgs): Promise<EditionArgs> {
        return new Promise<EditionArgs>((resolve, reject) => {
            this.zip.file("content.opf", this.opf.toBuffer(), {
                compression: "DEFLATE",
            });
            resolve(args);
        });
    }

    private addMimeType(args: EditionArgs): Promise<EditionArgs> {
        return new Promise<EditionArgs>((resolve, reject) => {
            this.zip.file(
                "mimetype",
                Buffer.from("application/epub+zip", "utf8"),
                { compression: "STORE" }
            );
            resolve(args);
        });
    }

    private addStylesheet(args: EditionArgs): Promise<EditionArgs> {
        return new Promise<EditionArgs>((resolve, reject) => {
            let css = args.theme.renderStylesheet(args);
            this.zip.file("stylesheet.css", css, { compression: "DEFLATE" });
            this.opf.manifests.push(
                new opf.OpfManifest("stylesheet", "stylesheet.css", "text/css")
            );
            args.logger.debug("Added stylesheet.css");
            resolve(args);
        });
    }

    private addTocNcx(args: EditionArgs): Promise<EditionArgs> {
        return new Promise<EditionArgs>((resolve, reject) => {
            this.zip.file("toc.ncx", this.ncx.toBuffer(), {
                compression: "DEFLATE",
            });
            resolve(args);
        });
    }

    private getFileId(path: string): string {
        const hash = crypto
            .createHash("sha1")
            .update(fs.readFileSync(path))
            .digest("hex");
        const id = `i${hash}`;

        return id;
    }

    private writeEpub(args: EditionArgs): Promise<EditionArgs> {
        // Pull out the zip archive as a buffer.
        let promise = new Promise<EditionArgs>((resolve, reject) => {
            // Figure out the filename we'll be writing.
            let epubFilename = path.join(
                args.rootDirectory,
                args.edition.outputDirectory!,
                args.edition.outputFilename!
            );

            args.logger.info(`Writing out EPUB: ${epubFilename}`);

            // Write it out using a stream.
            this.zip
                .generateNodeStream({ type: "nodebuffer", streamFiles: true })
                .pipe(fs.createWriteStream(epubFilename))
                .on("finish", () => resolve(args));
        });

        // Return the resulting promise.
        return promise;
    }
}
