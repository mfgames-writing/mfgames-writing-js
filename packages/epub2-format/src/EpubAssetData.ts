export interface EpubAssetData {
    /** The relative path to the asset on the disk. */
    path: string;

    /** The path inside the archive, defaults to the path. */
    archivePath?: string;

    /**
     * The mime type of the asset being added, defaults to
     * `application/octet-stream`.
     */
    mime?: string;
}
