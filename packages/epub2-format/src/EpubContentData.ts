import { ContentData } from "@mfgames-writing/contracts";

/**
 * Extends the content data to include additional information to use used by
 * the EPUB formatter.
 */
export interface EpubContentData extends ContentData {
    includeHeadersInNcx: {
        h1?: boolean;
        h2?: boolean;
        h3?: boolean;
        h4?: boolean;
        h5?: boolean;
    };
}
