# Moonfire Games Writing Formatter

`mfgames-writing-format` is a command-line framework for formatting Markdown-based books into a variety of formats using a JSON or YAML configuration file combined with plugins for themes and transformers. This is designed to have relatively little impact on the directory structure for the project and can run via `npm` or `make` easily.
