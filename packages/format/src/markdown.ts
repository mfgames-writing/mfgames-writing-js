import fs = require("mz/fs");
import { ContentArgs, ContentRenderCallback } from "@mfgames-writing/contracts";
import { loadModule } from "./plugins";
import * as markdownIt from "markdown-it";
import { smartypantsu } from "smartypants";

/**
 * Renders a Markdown file as HTML.
 */
export function renderContentMarkdown(
    content: ContentArgs
): Promise<ContentArgs> {
    return new Promise<ContentArgs>((resolve) => {
        // Set up a new markdown-it renderer with our defaults. We don't include
        // typographer because we use Smartypants instead which seems to handle
        // some edge cases better.
        var markdown = markdownIt({
            typographer: false,
            xhtmlOut: true,
        });

        if (content.edition.markdown) {
            // See if we have additional options.
            if (content.edition.markdown.options) {
                content.logger.debug(
                    "Setting additional markdown-it options",
                    content.edition.markdown.options
                );
                markdown.set(content.edition.markdown.options);
            }

            // See if we have additional extensions to load.
            if (content.edition.markdown.extensions) {
                for (const extension of content.edition.markdown.extensions) {
                    // Load the module into memory.
                    const extensionPackage = loadModule(
                        content.editionArgs,
                        extension.package,
                        false
                    );

                    if (!extensionPackage) {
                        content.logger.error(
                            "Cannot load Markdown extension",
                            extension
                        );
                        continue;
                    }

                    // Get the options and run any scripts.
                    let options: any = extension.options ?? {};

                    if (extension.script) {
                        content.logger.debug(
                            "Evaluating initialization script",
                            extension.package,
                            extension.script
                        );

                        const scriptModule = loadModule(
                            content.editionArgs,
                            extension.script
                        );
                        const newOptions = scriptModule(options);

                        content.logger.debug(
                            "Evaluated results of initialization script",
                            extension.package,
                            newOptions
                        );

                        options = newOptions ?? options;
                    }

                    // Combine everything together.
                    content.logger.debug(
                        "Loading Markdown extension",
                        extension.package,
                        options
                    );
                    markdown.use(extensionPackage, options);
                }
            }
        }

        // First process the code using Smartypants.
        content.logger.debug("formatting output with SmartyPants");
        const formatted = smartypantsu(content.text, 2);

        // Set up the theme overrides.
        setThemeRenderRule(content, content.theme.renderRule, markdown, "hr");
        setThemeRenderRule(
            content,
            content.theme.renderStrongOpen,
            markdown,
            "strong_open"
        );
        setThemeRenderRule(
            content,
            content.theme.renderStrongClose,
            markdown,
            "strong_close"
        );
        setThemeRenderRule(
            content,
            content.theme.renderEmphasisOpen,
            markdown,
            "em_open"
        );
        setThemeRenderRule(
            content,
            content.theme.renderEmphasisClose,
            markdown,
            "em_close"
        );
        setThemeRenderRule(
            content,
            content.theme.renderCodeSpan,
            markdown,
            "code_inline"
        );
        setThemeRenderRule(
            content,
            content.theme.renderBlockQuoteOpen,
            markdown,
            "blockquote_open"
        );
        setThemeRenderRule(
            content,
            content.theme.renderBlockQuoteClose,
            markdown,
            "blockquote_close"
        );

        // Process the markdown content.
        let html = markdown.render(formatted);

        // Markdown has an interesting quirk that a "break" is actually a
        // backslash followed by a space. However, trailing spaces are difficult
        // to work with when you use EditorConfig to remove trailing them
        // because no sane person can actually see them, so we just treat a
        // backslash at the end of the line as that explicit break.
        html = html.replace(/\\\n/g, "<br/>\n");

        // We want to tag paragraphs that only have a single image in them.
        html = html.replace(/<p><img([^>]+?)\s+\/><\/p>/g, (_r, i) => {
            // If we don't have a class, add it.
            if (i.indexOf("class=") < 0) {
                i += ' class=""';
            }

            //. Add the single image into the class.
            i = i.replace(/class=(.)/, "class=$1single-image ");

            // Return the results.
            return `<p class="single-image"><img${i} /></p>`;
        });

        // See if the theme wants to be able to manipulate the HTML.
        if (content.theme.transformHtml) {
            content.logger.debug("applying theme transformations");
            html = content.theme.transformHtml(content, html);
        }

        // Set the content and resolve this promise.
        content.text = html;

        resolve(content);
    });
}

function setThemeRenderRule(
    args: ContentArgs,
    override: ContentRenderCallback | undefined,
    markdown: markdownIt,
    ruleName: string
): void {
    if (!override) {
        return;
    }

    markdown.renderer.rules[ruleName] = (
        tokens,
        idx,
        options,
        env,
        renderer
    ) => {
        return override(args, tokens, idx, options, env, renderer);
    };
}
