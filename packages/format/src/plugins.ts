/* eslint @typescript-eslint/no-var-requires: 0 */

import { EditionArgs } from "@mfgames-writing/contracts";
import * as path from "path";

var loaded: { [index: string]: any } = {};

export function loadModule(
    args: EditionArgs,
    name: string,
    requireDefault: boolean = true
): any {
    // Report what we're doing. We want to avoid doing this a lot, so we only
    // show the message once.
    if (loaded[name]) {
        return loaded[name];
    }

    args.logger.debug(`Loading module: ${name}`);

    // If the module starts with a "./" or a "/", then we first treat it as a
    // local path. We still have to figure out the path, though, because require
    // is based on this library's path, not the source file.
    let mod = undefined;

    if (name.match(/^\.?\//)) {
        const localPath = path.resolve(args.rootDirectory, name);

        args.logger.debug(`Loading local module from ${localPath}`);
        mod = loadModulePath(args, localPath, requireDefault);
    }

    // Get the path to the standard NPM module.
    if (!mod) {
        mod = loadModulePath(args, name, requireDefault);
    }

    // Try the modules from the current working directory.
    let rootPath = path.join(process.cwd(), "node_modules");

    if (!mod) {
        mod = loadModulePath(args, path.join(rootPath, name), requireDefault);
    }

    // If we can't find it, then blow up.
    if (!mod) {
        args.logger.error(`Cannot find module: ${name}`);
        throw `Cannot find module: ${name}`;
    }

    // Return the resulting module.
    loaded[name] = mod;
    return mod;
}

function loadModulePath(
    args: EditionArgs,
    modulePath: string,
    requireDefault: boolean
): any {
    try {
        let results = require(modulePath);

        if (!requireDefault) {
            return results;
        }

        if (results.default) {
            args.logger.debug(`Loaded ${modulePath}`);
            return results.default;
        }

        args.logger.warning(`Found ${modulePath} but missing default export`);

        return null;
    } catch (exception) {
        return null;
    }
}
