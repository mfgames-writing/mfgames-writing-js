import { PublicationArgs, IncludeData } from "@mfgames-writing/contracts";
import * as path from "path";
import { promises as fs } from "fs";
import * as set from "lodash.set";

/**
 * Processes the publication file and handles any include statements that
 * may be included.
 */
export async function mergeIncludes(args: PublicationArgs): Promise<void> {
    // If we don't have an include statement, then nothing to do.
    if (!args.publication.includes) {
        args.logger.debug("no include statements to process");
        return;
    }

    // Loop through each include and process them.
    for (const include of args.publication.includes) {
        await mergeInclude(args, include);
    }
}

async function mergeInclude(
    args: PublicationArgs,
    include: IncludeData
): Promise<void> {
    // Report what we're doing.
    const publication = args.publication;
    const directory = args.rootDirectory;
    const log = args.logger;

    log.info("merging include", include);

    // The first thing is to figure out what we're going to be including.
    // We start with a `{}` because everything could be optional and we
    // want to allow multiples.
    let data = {};

    if (include.json) {
        // Get the normalized path to the json.
        const jsonPath = path.isAbsolute(include.json)
            ? include.json
            : path.join(directory, include.json);
        const rawJson = await fs.readFile(jsonPath);
        const jsonData = JSON.parse(rawJson.toString());

        log.trace("including JSON:", jsonPath, jsonData);

        data = { ...data, ...jsonData };
    }

    log.trace("final merged object", data);

    // We've gotten all the data combined together, now merge it.
    for (const merge of [include.merge].flat()) {
        set(publication, merge, data);
        log.debug("merging into", merge);
    }
}
