import * as path from "path";
import fs = require("mz/fs");
import {
    EditionArgs,
    ContentArgs,
    ContentData,
} from "@mfgames-writing/contracts";
import incremental = require("incremental");
import liquid = require("liquid-node");
import yamlFrontMatter = require("yaml-front-matter");
import zpad = require("zpad");
import { processImages } from "./image";
import { loadModule } from "./plugins";
import { renderContentMarkdown } from "./markdown";

class AppendArgs {
    public editionArgs: EditionArgs;
    public contentIndex: number = 0;

    constructor(editionArgs: EditionArgs) {
        this.editionArgs = editionArgs;
    }
}

/**
 * Goes through the contents list of the publication and expands out any source
 * patterns from the results, populating them into `args.contents`.
 */
export async function appendContents(args: EditionArgs): Promise<EditionArgs> {
    var appendArgs = new AppendArgs(args);
    let promise = Promise.resolve(appendArgs);

    if (args.publication.contents) {
        for (let content of args.publication.contents) {
            promise = promise.then(() => appendContent(appendArgs, content));
        }
    }

    const results = await promise;

    return results.editionArgs;
}

/**
 * Appends the contents of a single item to `args.contents`.
 */
function appendContent(
    args: AppendArgs,
    originalContent: ContentData
): Promise<AppendArgs> {
    // We need to work with a copy of the content.
    let content = { ...originalContent };

    // See if this content has been filtered out.
    if (content.exclude) {
        if (
            content.exclude.editions &&
            content.exclude.editions.indexOf(args.editionArgs.name) >= 0
        ) {
            args.editionArgs.logger.debug(
                "excluding edition",
                args.editionArgs.name
            );
            return Promise.resolve(args);
        }
    }

    // Otherwise, create the contents and process theme. We need to sequence the
    // content from the beginning to give them a unique identifier.
    return new Promise<AppendArgs>((resolve) => {
        // Figure out the directory we'll be processing for this.
        let directory = content.directory
            ? path.join(args.editionArgs.rootDirectory, content.directory)
            : args.editionArgs.rootDirectory;

        // Figure out if we have a parent.
        let parentArgs =
            content.parent && "ContentArgs" in content.parent.process
                ? content.parent.process["ContentArgs"]
                : undefined;

        // If we don't have a source, then we intend to generate the results.
        let patternMatch = content.source
            ? content.source.match(/^\/(.+)\/$/)
            : undefined;

        if (patternMatch) {
            // If this starts and stops with a "/", it is a regex. If we have that, then
            // we scan the directory to get the files. Otherwise, we just use the source
            // as a straight filename.

            // Go through all the files in the directory.
            let sourceRegex = new RegExp(patternMatch[1]);

            for (let file of fs.readdirSync(directory)) {
                if (sourceRegex.test(file)) {
                    // Create a copy of the content with the pattern populated.
                    let filename = file;
                    let patternContent: ContentData = {
                        ...content,
                        directory: directory,
                        number: content["number"],
                        source: filename,
                        process: {},
                    };

                    // Add the promise to handle this into the pipeline.
                    let contentArgs = new ContentArgs(
                        args.editionArgs,
                        patternContent,
                        ++args.contentIndex
                    );
                    contentArgs.parent = parentArgs;
                    args.editionArgs.contents.push(contentArgs);

                    // Only the first one in a match is going to be the first one.
                    // Likewise, we reset the forced page counter.
                    content.start = false;
                    content.page = undefined;

                    // If we have a number, then increment it.
                    if (content.number) {
                        content.number = incremental(content.number);
                    }
                }
            }
        } else {
            let contentArgs = new ContentArgs(
                args.editionArgs,
                content,
                ++args.contentIndex
            );
            contentArgs.parent = parentArgs;
            args.editionArgs.contents.push(contentArgs);
            content.process["ContentArgs"] = contentArgs;
        }

        // Finish up the promise.
        resolve(args);
    });
}

/**
 * Loads information about the contents including parsing the files into memory
 * and loading metadata into the contents.
 */
export async function loadContents(args: EditionArgs): Promise<EditionArgs> {
    // Loading can be done in any order. While normally we don't want to load
    // everything into memory, typically these books are small enough to fit
    // in the space so we are going to just do it.
    let promises: Promise<ContentArgs>[] = [];

    for (let content of args.contents) {
        promises.push(loadContent(content));
    }

    // Combine everything together at the end.
    await Promise.all(promises);

    return args;
}

/**
 * Creates a promise that constructs a loading pipeline for the various content
 * elements used for the edition.
 */
async function loadContent(args: ContentArgs): Promise<ContentArgs> {
    // Start with a basic promise, this makes it easier to build the chain.
    let promise = Promise.resolve(args);

    // If we don't have a source, then we have nothing to load.
    if (!args.contentData.source) {
        const content = await promise;

        return loadContentGeneration(content);
    }

    // Figure out the directory and filename for this content.
    let ext = args.extension;

    // If this is an image, then just replace it with an HTML to include it.
    if (ext === ".jpg" || ext === ".png") {
        promise = promise.then(loadContentImage);
    }

    // If this is Markdown, then we need to render it.
    if (
        ext === ".md" ||
        ext === ".markdown" ||
        ext === ".htm" ||
        ext === ".html"
    ) {
        promise = promise.then(loadContentText);
    }

    // Return the resulting promise.
    return promise;
}

/**
 * Loads a direct image by wrapping a HTML file around it and then faking the
 * results.
 */
function loadContentImage(content: ContentArgs): Promise<ContentArgs> {
    return new Promise<ContentArgs>((resolve) => {
        // Figure out the alt tag for the image. This is required for EPUB.
        let isCover = content.element === "cover";
        let alt = isCover ? content.edition.title + " cover" : "";

        // Set up the content elements with the faked HTML.
        let filename = content.filename;

        content.source = content.source + ".html";
        content.metadata = {
            title: isCover ? "Cover" : "Image",
            element: content.element,
            directory: content.directory,
        };
        content.text = `<p class="cover">
            <img src="${filename}" alt="${alt}" />
        </p>`;

        // Resolve the promise.
        content.logger.info(`Wrapping image into ${content.source}`);
        resolve(content);
    });
}

/**
 * Loads the text and YAML header from a given text file.
 */
function loadContentText(content: ContentArgs): Promise<ContentArgs> {
    // Start the promise chain with reading files into memory.
    var promise: Promise<any> = fs.readFile(content.filename);

    // Pull out the contents into a text buffer.
    promise = promise.then((buffer) => {
        const parsed = yamlFrontMatter.loadFront(buffer, {
            contentKeyName: "_",
        });
        let metadata = { ...parsed };
        let text = metadata["_"];
        delete (metadata as any)["_"];

        content.metadata = { ...content.metadata, ...metadata };
        content.text = text;
        content.buffer = buffer;

        return content;
    });

    // If we have a pipeline, then we want to funnel the content through each
    // one in turn.
    if (content.pipeline) {
        for (let pipeline of content.pipeline) {
            let moduleName = pipeline.module;
            let pipelineModule = loadModule(content.editionArgs, moduleName);
            let pipelineInstance = pipelineModule(pipeline);

            promise = promise.then((c) => pipelineInstance.process(c));
        }
    }

    // Return the resulting promise chain.
    return promise;
}

function loadContentGeneration(content: ContentArgs): Promise<ContentArgs> {
    return new Promise<ContentArgs>((resolve) => {
        switch (content.element) {
            case "toc":
                var params: any = content.contentData;
                var title = params.title ? params.title : "Table of Contents";
                content.metadata.title = title;
                break;
            default:
                throw Error(
                    `Cannot generate content for element type ${content.element}.`
                );
        }

        resolve(content);
    });
}

/**
 * Renders the various content for inclusion with the format.
 */
export async function renderContents(args: EditionArgs): Promise<EditionArgs> {
    // Renders each of the contents and prepares them to be added to the format.
    // This has to be in order because of the NCX and OPF generation.
    let promise: Promise<any> = Promise.resolve(args);

    for (let content of args.contents) {
        promise = promise.then((e) => renderContent(content));
    }

    // Combine everything together at the end.
    await promise;

    return args;
}

/**
 * Renders a single content and prepares it for including into the format.
 */
function renderContent(args: ContentArgs): Promise<ContentArgs> {
    // Start with a basic promise, this makes it easier to build the chain.
    let promise = Promise.resolve(args);

    // If we don't have a source, then we have special rules for formatting.
    if (!args.contentData.source) {
        // Parse the input and create the basic HTML out of it.
        promise = promise.then(renderContentGeneration);
        promise = promise.then((a) => args.theme.renderHtml(a));

        // The above rendering doesn't include things like <html> or style. We
        // only add these if we are wrapping each file individually. For single
        // document files, such as Word documents or some PDF, we don't do this.
        let settings = args.format.getSettings();

        if (settings.wrapIndividualFiles) {
            promise = promise.then((a) => args.theme.renderLayout(a));
        }

        // Go through the images and process them. Once we do, we hand the
        // information over to the formatter for inclusion.
        promise = promise.then(() => processImages(args));

        // Give the HTML to the formatter to do whatever it needs to do.
        promise = promise.then((a) => args.format.addHtml(a));
        return promise;
    }

    // Figure out the directory and filename for this content.
    let ext = args.extension;

    // If we are identified to have a liquid template inside, we need to
    // resolve that template first and rebuild the text.
    if (args.contentData.liquid) {
        promise = promise.then(renderContentLiquid);
    }

    // If this is Markdown, then we need to render it.
    if (ext === ".md" || ext === ".markdown") {
        promise = promise.then(renderContentMarkdown);
    }

    // If this is Markdown or HTML, then render it via the theme.
    if (
        ext === ".md" ||
        ext === ".markdown" ||
        ext === ".htm" ||
        ext === ".html"
    ) {
        // Figure out if we need to wrap the individual file.
        var settings = args.format.getSettings();

        promise = promise.then((a) => args.theme.renderHtml(a));

        if (settings.wrapIndividualFiles) {
            promise = promise.then((a) => args.theme.renderLayout(a));
        }

        // Go through the images and process them. Once we do, we hand the
        // information over to the formatter for inclusion.
        promise = promise.then(() => processImages(args));

        // Add the rendered HTML to the list.
        promise = promise.then((a) => args.format.addHtml(a));
    }

    // Return the resulting promise.
    return promise;
}

/**
 * Renders the contents of the file as a Liquid template and resolves it.
 */
function renderContentLiquid(content: ContentArgs): Promise<ContentArgs> {
    return new Promise<ContentArgs>((resolve) => {
        // Report what we are doing.
        content.logger.info("Rendering Liquid contents");

        // Create a template from the text of the contents. The final one is
        // actually a promise.
        let engine = new liquid.Engine();
        let template = content.text;
        let promise = engine
            .parse(template)
            .then((t: any) => {
                const parameters = {
                    content: content.metadata,
                    edition: content.edition,
                    theme: this,
                };
                parameters.content.depth = content.depth;

                return t.render(parameters);
            })
            .then((rendered: any) => {
                content.text = rendered;
                return content;
            });
        resolve(promise);
    });
}

/**
 * Renders content based on the element type.
 */
function renderContentGeneration(content: ContentArgs): Promise<ContentArgs> {
    // Figure out the type.
    switch (content.element) {
        case "toc":
            return content.theme.renderTableOfContents(content);
        default:
            throw Error(
                `Cannot generate content for element type ${content.element}.`
            );
    }
}

/**
 * The components pulled out after the call to parseAttributes.
 */
interface ParseAttributesResults {
    text: string;
    attributes: string;
    classes: string[];
    id: string | undefined;
}

/**
 * Parses a string to pull out an attribute string ({:...}) from the given
 * text and return the parsed results.
 *
 * @param {string} text The text to parse.
 * @param {string?} quote The quote character to use, defaults to '"'.
 * @returns {ParseAttributesResults} The split out results.
 */
function parseAttributes(
    text: string,
    quote: string = '"'
): ParseAttributesResults {
    let classes: string[] = [];
    let id = undefined;
    let removed = text.replace(/\s*\{:(.*?)\}/, (match, attributes) => {
        // Split the attributes on spaces.
        let attrs = attributes.trim().split(/\s+/);

        for (let attr of attrs) {
            // Grab the starting key and the rest of it.
            let m = attr.match(/^([.#])(.*)$/);

            if (m) {
                switch (m[1]) {
                    case ".":
                        classes.push(m[2]);
                        break;
                    case "#":
                        id = m[2];
                        break;
                }
            }
        }

        // Just remove the attributes.
        return "";
    });

    // Format the attributes.
    let c = "";

    if (classes.length > 0) {
        c += " class=" + quote + classes.join(" ") + quote;
    }

    if (id) {
        c += " id=" + quote + id + quote;
    }

    // Build up the results.
    return {
        text: removed,
        attributes: c,
        classes: classes,
        id: id,
    };
}
