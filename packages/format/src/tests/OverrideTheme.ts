import {
    Theme,
    EditionArgs,
    ContentArgs,
    ContentTheme,
} from "@mfgames-writing/contracts";
import Token = require("markdown-it/lib/token");

export class OverrideTheme implements Theme {
    stylesheetFileName: string;

    renderEmphasisOpen(): string {
        return "OVERRIDE-EMPHASIS[";
    }

    renderEmphasisClose(): string {
        return "]";
    }

    renderStrongOpen(): string {
        return "OVERRIDE-STRONG(";
    }

    renderStrongClose(): string {
        return ")";
    }

    renderCodeSpan(_: ContentArgs, tokens: Token[], idx: number): string {
        return "OVERRIDE-CODE{" + tokens[idx].content + "}";
    }

    renderRule(): string {
        return "OVERRIDE-RULE";
    }

    renderBlockQuoteOpen(): string {
        return "BLOCKQUOTE-BEGIN\n";
    }

    renderBlockQuoteClose(): string {
        return "\nBLOCKQUOTE-END\n";
    }

    start(args: EditionArgs): Promise<EditionArgs> {
        return Promise.resolve(args);
    }

    finish(args: EditionArgs): Promise<EditionArgs> {
        return Promise.resolve(args);
    }

    renderHtml(args: ContentArgs): Promise<ContentArgs> {
        return Promise.resolve(args);
    }

    renderLayout(args: ContentArgs): Promise<ContentArgs> {
        return Promise.resolve(args);
    }

    renderNavigationTitle(args: ContentArgs): string {
        throw new Error("Method not implemented.");
    }

    renderStylesheet(
        args: EditionArgs,
        mode?: string | string[] | undefined
    ): Buffer {
        return Buffer.alloc(0, "");
    }

    renderTableOfContents(args: ContentArgs): Promise<ContentArgs> {
        return Promise.resolve(args);
    }

    getContentTheme(content: ContentArgs): ContentTheme {
        throw new Error("Method not implemented.");
    }
}
