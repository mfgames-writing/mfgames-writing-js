import * as expect from "expect";
import * as path from "path";

const simplePath = path.join(__dirname, "simple.yaml");

describe("test environment", function () {
    it("works", function (callback: () => void) {
        expect(1).toBe(1);
        callback();
    });
});
