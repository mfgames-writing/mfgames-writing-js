import { OverrideTheme } from "./OverrideTheme";

export default function load() {
    return new OverrideTheme();
}
