import * as expect from "expect";
import * as path from "path";
import { runBuild } from "../build";
import * as tracer from "tracer";
import * as fs from "fs";
import * as striptags from "striptags";

const simplePath = path.join(__dirname, "simple.yaml");

const logger = tracer.colorConsole({
    format: "{{message}}",
    level: "error",
});

describe("theme overrides", function () {
    it("basic options", function (callback: () => void) {
        const name = getTestName();
        const html = `${__dirname}/${name}.html`;

        runBuild({ c: simplePath, _: [] }, { name }, logger)
            .then((_) => {
                const text = readHtml(html);
                expect(text).toContain("This is the first paragraph.");
            })
            .then((_) => fs.unlinkSync(html))
            .then((_) => callback());
    });

    it("rule", function (callback: () => void) {
        const name = getTestName();
        const html = `${__dirname}/${name}.html`;

        runBuild({ c: simplePath, _: [] }, { name }, logger)
            .then((_) => {
                const text = readHtml(html);
                expect(text).toContain("OVERRIDE-RULE");
            })
            .then((_) => fs.unlinkSync(html))
            .then((_) => callback());
    });

    it("strong", function (callback: () => void) {
        const name = getTestName();
        const html = `${__dirname}/${name}.html`;

        runBuild({ c: simplePath, _: [] }, { name }, logger)
            .then((_) => {
                const text = readHtml(html);
                expect(text).toContain("OVERRIDE-STRONG(bold)");
            })
            .then((_) => fs.unlinkSync(html))
            .then((_) => callback());
    });

    it("emphasis", function (callback: () => void) {
        const name = getTestName();
        const html = `${__dirname}/${name}.html`;

        runBuild({ c: simplePath, _: [] }, { name }, logger)
            .then((_) => {
                const text = readHtml(html);
                expect(text).toContain("OVERRIDE-EMPHASIS[italic]");
            })
            .then((_) => fs.unlinkSync(html))
            .then((_) => callback());
    });

    it("code", function (callback: () => void) {
        const name = getTestName();
        const html = `${__dirname}/${name}.html`;

        runBuild({ c: simplePath, _: [] }, { name }, logger)
            .then((_) => {
                const text = readHtml(html);
                expect(text).toContain("OVERRIDE-CODE{code}");
            })
            .then((_) => fs.unlinkSync(html))
            .then((_) => callback());
    });

    it("blockquote", function (callback: () => void) {
        const name = getTestName();
        const html = `${__dirname}/${name}.html`;

        runBuild({ c: simplePath, _: [] }, { name }, logger)
            .then((_) => {
                const text = readHtml(html);
                expect(text).toContain("BLOCKQUOTE-BEGIN");
                expect(text).toContain("BLOCKQUOTE-END");
            })
            .then((_) => fs.unlinkSync(html))
            .then((_) => callback());
    });
});

function getTestName(): string {
    return (expect as any).getState().currentTestName.replace(/\s+/g, "-");
}

function readHtml(path: string): string {
    return striptags(
        fs
            .readFileSync(path)
            .toString()
            .replace(/<style>.*?<\/style>/gs, "")
    )
        .split("\n")
        .filter((x) => !x.match(/^\s*$/))
        .map((x) => x.trim())
        .join("\n");
}
