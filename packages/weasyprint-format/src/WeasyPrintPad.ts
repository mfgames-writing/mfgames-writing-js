export enum WeasyPrintPad {
    /** Don't perform any padding. */
    none = "none",

    /** Pad to the left page so the next is on the right. */
    left = "left",
}
