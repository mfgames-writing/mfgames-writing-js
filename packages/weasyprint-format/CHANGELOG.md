# [@mfgames-writing/weasyprint-v4.4.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.3.0...weasyprint-4.4.0) (2021-07-24)

# [@mfgames-writing/weasyprint-v4.3.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.2.0...weasyprint-v4.3.0) (2021-07-17)

# [@mfgames-writing/weasyprint-v4.2.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.1.9...weasyprint-v4.2.0) (2021-07-17)

# [@mfgames-writing/weasyprint-v4.1.9](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.1.8...weasyprint-v4.1.9) (2021-07-17)

# [@mfgames-writing/weasyprint-v4.1.8](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.1.7...weasyprint-v4.1.8) (2021-07-17)

# [@mfgames-writing/weasyprint-v4.1.7](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.1.6...weasyprint-v4.1.7) (2021-03-15)

# [@mfgames-writing/weasyprint-v4.1.6](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.1.5...weasyprint-v4.1.6) (2021-03-07)

# [@mfgames-writing/weasyprint-v4.1.5](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.1.4...weasyprint-v4.1.5) (2021-03-06)

# [@mfgames-writing/weasyprint-v4.1.4](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.1.3...weasyprint-v4.1.4) (2021-03-06)

# [@mfgames-writing/weasyprint-v4.1.3](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.1.2...weasyprint-v4.1.3) (2021-03-06)

# [@mfgames-writing/weasyprint-v4.1.2](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.1.1...weasyprint-v4.1.2) (2021-03-05)


### Bug Fixes

* **weasyprint:** made the WeasyPrint version parser more tolerant ([636a54b](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/636a54b49e279759dcddaec715c90cc65ef0dd26))

# [@mfgames-writing/weasyprint-v4.1.1](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.1.0...weasyprint-v4.1.1) (2021-02-04)

# [@mfgames-writing/weasyprint-v4.1.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/weasyprint-v4.0.8...weasyprint-v4.1.0) (2021-02-03)


### Bug Fixes

* correcting mistakes from monorepo merging and refactoring ([7d44c63](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/7d44c632a51a05a69bb1cd8dbb81a8b603a6d0d1))

# [@mfgames-writing/weasyprint-v4.0.8](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/weasyprint-v4.0.7...weasyprint-v4.0.8) (2020-07-26)


### Bug Fixes

* switched out dotted for lodash.get properly ([ae6cdd0](https://git.sr.ht/~dmoonfire/mfgames-writing-js/commit/ae6cdd071b440f23ba825fb06fe71ad16f9923a2))

## [3.0.2](https://gitlab.com/mfgames-writing/mfgames-writing-weasyprint-js/compare/v3.0.1...v3.0.2) (2018-08-11)


### Bug Fixes

* adding package management ([3e99a1a](https://gitlab.com/mfgames-writing/mfgames-writing-weasyprint-js/commit/3e99a1a))
