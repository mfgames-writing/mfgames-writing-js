import {
    ContentArgs,
    ContentData,
    EditionArgs,
    FormatImageRequest,
    FormatImageResponse,
    Formatter,
    FormatterSettings,
} from "@mfgames-writing/contracts";
import * as fs from "fs";
import * as path from "path";
import * as zpad from "zpad";

export function loadHtmlFormatter() {
    return new HtmlFormatter();
}

export class HtmlFormatter implements Formatter {
    private buffer: string = "";
    private stylesheet: string;
    private lastContent: ContentArgs;
    private contentCounter = 0;

    public getSettings(): FormatterSettings {
        var settings = new FormatterSettings();
        settings.wrapIndividualFiles = false;
        return settings;
    }

    public start(args: EditionArgs): Promise<EditionArgs> {
        // Start with a simple promise because it makes it easier to maintain.
        let promise = Promise.resolve(args);

        // We want to grab all of the stylesheet since we are going to replace
        // it inline in the resulting HTML.
        promise = promise.then((a) => {
            this.stylesheet = args.theme
                .renderStylesheet(args, ["html"])
                .toString();
            return args;
        });

        // Return the resulting promise.
        return promise;
    }

    public addHtml(content: ContentArgs): Promise<ContentArgs> {
        this.lastContent = content;

        return Promise.resolve(content).then((c) => {
            this.buffer += c.text;
            return c;
        });
    }

    public addImage(
        content: ContentArgs,
        image: FormatImageRequest
    ): FormatImageResponse {
        // If we don't have an image, skip it.
        if (!image.buffer) {
            content.logger.debug(`Missing image: ${image.imagePath}`);
            throw new Error();
        }

        // Convert the image into base 64 to we can embed it directly.
        const encoded = image.buffer.toString("base64");
        const href = "data:image/png;base64," + encoded;

        // Return the resulting data.
        return {
            include: true,
            href: href,
            callback: (a: any) => content,
        };
    }

    public finish(args: EditionArgs): Promise<EditionArgs> {
        // Add in the various final steps we need. We start with a simple
        // promise and then chain it so we can easily maintain this logic.
        let promise: Promise<any> = Promise.resolve(args);

        // First we need to wrap the contents of the entire HTML in chrome.
        // With the way this is written, we have to fake the ContentArgs to
        // represent the entire buffer as a single object.
        promise = promise.then((a) => {
            var contentData = {
                id: `h${zpad(this.contentCounter++, 4)}`,
                element: this.lastContent.metadata.element,
                directory: this.lastContent.metadata.directory,
                source: this.lastContent.metadata.source,
                depth: this.lastContent.metadata.depth,
                text: "",
            } as ContentData;
            var contentArgs = new ContentArgs(args, contentData);
            contentArgs.text = this.buffer;

            return contentArgs;
        });
        promise = promise.then((c) => args.theme.renderLayout(c));

        // Write out the resulting HTML file.
        promise = promise.then((c) => {
            // Figure out the filename we'll be writing.
            let htmlFilename = path.join(
                args.rootDirectory,
                args.edition.outputDirectory!,
                args.edition.outputFilename!
            );

            args.logger.info(`Writing out HTML: ${htmlFilename}`);

            // Write out the file and return the results. While we do that,
            // we also inline the entire stylesheet so we can have a single,
            // self-contained HTML file.
            fs.writeFileSync(
                htmlFilename,
                c.text.replace(/<link.*?>/, `<style>${this.stylesheet}</style>`)
            );

            return args;
        });

        // Return the resulting promise.
        return promise;
    }

    private addStylesheet(args: EditionArgs): Promise<EditionArgs> {
        return Promise.resolve(args);
        // TODO return new Promise<EditionArgs>((resolve, reject) => {
        // TODO     let css = args.theme.renderStylesheet(args);
        // TODO     this.zip.file(
        // TODO         "stylesheet.css",
        // TODO         css,
        // TODO         { compression: "DEFLATE" });
        // TODO     this.opf.addManifest("stylesheet", "stylesheet.css", "text/css");
        // TODO     args.logger.debug("Added stylesheet.css");
        // TODO     resolve(args);
        // TODO });
    }

    private writeHtml(args: EditionArgs): Promise<EditionArgs> {
        return Promise.resolve(args);
        // TODO // Pull out the zip archive as a buffer.
        // TODO let promise = new Promise<EditionArgs>((resolve, reject) => {
        // TODO     // Figure out the filename we'll be writing.
        // TODO     let HtmlFilename = path.join(
        // TODO         args.rootDirectory,
        // TODO         args.edition.outputDirectory,
        // TODO         args.edition.outputFilename);

        // TODO     args.logger.info(`Writing out Html: ${HtmlFilename}`);

        // TODO     // Write it out using a stream.
        // TODO     this.zip
        // TODO         .generateNodeStream({ type: 'nodebuffer', streamFiles: true })
        // TODO         .pipe(fs.createWriteStream(HtmlFilename))
        // TODO         .on("finish", () => resolve(args));
        // TODO });

        // TODO // Return the resulting promise.
        // TODO return promise;
    }
}
