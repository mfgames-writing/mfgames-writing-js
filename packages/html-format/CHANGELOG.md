# [@mfgames-writing/html-v0.5.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.4.0...html-0.5.0) (2021-07-24)

# [@mfgames-writing/html-v0.4.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.3.0...html-v0.4.0) (2021-07-17)

# [@mfgames-writing/html-v0.3.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.2.9...html-v0.3.0) (2021-07-17)

# [@mfgames-writing/html-v0.2.9](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.2.8...html-v0.2.9) (2021-07-17)

# [@mfgames-writing/html-v0.2.8](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.2.7...html-v0.2.8) (2021-07-17)

# [@mfgames-writing/html-v0.2.7](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.2.6...html-v0.2.7) (2021-03-15)


### Bug Fixes

* **html:** fixed loading issues ([bfdbff1](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/bfdbff17b9a303bda52d59b89471cb3c38a13b0c))

# [@mfgames-writing/html-v0.2.6](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.2.5...html-v0.2.6) (2021-03-07)

# [@mfgames-writing/html-v0.2.5](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.2.4...html-v0.2.5) (2021-03-06)

# [@mfgames-writing/html-v0.2.4](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.2.3...html-v0.2.4) (2021-03-06)

# [@mfgames-writing/html-v0.2.3](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.2.2...html-v0.2.3) (2021-03-06)

# [@mfgames-writing/html-v0.2.2](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.2.1...html-v0.2.2) (2021-03-05)

# [@mfgames-writing/html-v0.2.1](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.2.0...html-v0.2.1) (2021-02-04)

# [@mfgames-writing/html-v0.2.0](https://gitlab.com/mfgames-writing/mfgames-writing-js/compare/html-v0.1.8...html-v0.2.0) (2021-02-03)


### Bug Fixes

* correcting mistakes from monorepo merging and refactoring ([7d44c63](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/7d44c632a51a05a69bb1cd8dbb81a8b603a6d0d1))

# [@mfgames-writing/html-v0.1.8](https://git.sr.ht/~dmoonfire/mfgames-writing-js/compare/html-v0.1.7...html-v0.1.8) (2020-07-26)
