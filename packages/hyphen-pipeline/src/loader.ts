export * from "./index";
import { loadHyphenPlugin } from "./hyphen";
export default loadHyphenPlugin;
