import { ContentArgs, ContentPipeline } from "@mfgames-writing/contracts";
import createHyphenator = require("hyphen");

export function loadHyphenPlugin(args: any): HyphenPlugin {
    // Figure out which language pattern we need to use.
    var languageCode = args.lang ? args.lang : "en-us";
    const language = require(`hyphen/patterns/${languageCode}`);

    if (!language) {
        throw new Error("Cannot find language module for " + languageCode);
    }

    // Normalize the other variables.
    var exceptions = args.exceptions ? args.exceptions : [];
    var patterns = args.patterns ? args.patterns : [];

    if (exceptions && exceptions.length > 0) {
        language.exceptions = exceptions;
    }

    if (patterns && patterns.length > 0) {
        language.patterns = patterns;
    }

    // See if we have exclusion patterns, these are regular expressions that
    // we don't want to perform the pattern matching.
    var exclude: RegExp[] = [];

    if (args.exclude) {
        for (let ex of args.exclude) {
            exclude.push(new RegExp(`(${ex})`));
        }
    }

    // Create the hyphenator using the character code for a soft-hyphen since
    // EPUBs can't handle `&shy;` without declaring the entity.
    let hyphenate = createHyphenator(language, { hyphenChar: "&#173;" });

    return new HyphenPlugin(hyphenate, exclude);
}

export class HyphenPlugin implements ContentPipeline {
    private hyphenate: any;
    private exclude: RegExp[];

    constructor(hyphenate: any, exclude: RegExp[]) {
        this.hyphenate = hyphenate;
        this.exclude = exclude;
    }

    public process(content: ContentArgs): Promise<ContentArgs> {
        return new Promise<ContentArgs>((resolve) => {
            // Break apart the words, but only if we have some.
            if (!content.text) {
                return resolve(content);
            }

            content.text = this.hyphenate(content.text);

            // The hyphenate package doesn't handle attributes or markup at all
            // (e.g., it tries to hyphenate XML attributes). We need to go
            // through and undo the work inside the attributes.
            content.text = content.text.replace(
                /({:[^}]+})/g,
                function (attributes) {
                    attributes = attributes.replace(/&#173;/g, "");
                    return attributes;
                }
            );

            // We also need to undo the work inside the URL part of links
            // because we should never be breaking apart those strings.
            content.text = content.text.replace(
                /(\[[^\]]*\]\()(.*?)\)/g,
                function (_, text, url, close) {
                    url = url.replace(/&#173;/g, "");
                    return text + url + ")";
                }
            );

            // Loop through the exclude patterns and remove the expressions.
            for (let pattern of this.exclude) {
                content.text = content.text.replace(
                    pattern,
                    function (matched) {
                        matched = matched.replace(/&#173;/g, "");
                        return matched;
                    }
                );
            }

            // Finish up the results.
            resolve(content);
        });
    }
}
