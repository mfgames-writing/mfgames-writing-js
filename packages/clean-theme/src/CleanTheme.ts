import * as liquid from "@mfgames-writing/liquid-theme";
import path = require("path");
import { ContentArgs, ContentTheme } from "@mfgames-writing/contracts";

export class CleanTheme extends liquid.LiquidTheme {
    constructor() {
        const templateDirectory = path.join(__dirname, "..", "templates");
        const styleDirectory = path.join(__dirname, "..", "styles");
        super(templateDirectory, styleDirectory);
        this.pandocSettings = {
            dataDirectory: templateDirectory,
        };
    }

    public getContentTheme(content: ContentArgs): ContentTheme {
        // Start with the defaults.
        var results: any = super.getContentTheme(content);

        // Figure out the defaults for types.
        switch (content.element) {
            case "legal":
            case "title":
                results.weasyprint = {
                    pad: "none",
                };
        }

        // Return the theme elements.
        return results;
    }
}
