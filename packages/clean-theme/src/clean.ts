import { CleanTheme } from "./CleanTheme";

export function loadCleanTheme() {
    var theme = new CleanTheme();
    return theme;
}
