{
  description =
    "A framework for publishing Markdown files in a variety of formats.";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = pkgs.mkShell {
          buildInputs = [
            pkgs.nodejs-16_x
            pkgs.nixfmt
            pkgs.python27Full
            pkgs.python39Full
            pkgs.yarn
            pkgs.nodePackages.lerna
          ];
        };
      });
}
