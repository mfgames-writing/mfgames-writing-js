// Set up `marked` with common settings. We wrap the <img/> renderer
// to include a trailing tag. We also give the formatter a chance
// to manipulate the results before passing it in.
let renderer = new marked.Renderer();
let renderer2 = new marked.Renderer();

renderer.hr = () => {
    return content.theme.renderRule
        ? content.theme.renderRule(content)
        : renderer2.hr();
};
renderer.strong = (text) => {
    return content.theme.renderStrong
        ? content.theme.renderStrong(content, text)
        : renderer2.strong(text);
};
renderer.em = (text) => {
    return content.theme.renderEmphasis
        ? content.theme.renderEmphasis(content, text)
        : renderer2.em(text);
};
renderer.codespan = (text) => {
    return content.theme.renderCodeSpan
        ? content.theme.renderCodeSpan(content, text)
        : renderer2.codespan(text);
};
renderer.del = (text) => {
    return content.theme.renderDelete
        ? content.theme.renderDelete(content, text)
        : renderer2.del(text);
};
renderer.link = (href, title, text) => {
    return content.theme.renderLink
        ? content.theme.renderLink(content, href, title, text)
        : renderer2.link(href, title, text);
};
renderer.blockquote = (quote) => {
    return content.theme.renderBlockquote
        ? content.theme.renderBlockquote(content, quote)
        : renderer2.blockquote(quote);
};
renderer.image = (href, title, text) => {
    // We allow attributes in the href of the tag.
    let results = parseAttributes(href);

    // Call the original img implementation but close the tag to
    // make it proper XHTML.
    let html = renderer2.image(results.text, title, text);
    html = html.replace(/>$/, results.attributes + " />");

    // See if we have any other post processing.
    if (content.theme.processImage) {
        html = content.theme.processImage(content, html);
    }

    return html;
};
renderer.paragraph = (text) => {
    // Figure out if we have any attributes associated with this
    // paragraph. If we do, then parse them out.
    var results = parseAttributes(text);

    // Let the overriding theme alter the paragraph to allow for
    // letterines or special formatting.
    let para = `<p${results.attributes}>${results.text}</p>\n`;

    if (content.theme.processParagraph) {
        para = content.theme.processParagraph(content, para);
    }

    // Write out the results.
    return para;
};
