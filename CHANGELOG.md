# 1.0.0 (2021-02-03)


### Bug Fixes

* added package management ([917e1f9](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/917e1f94d365567f6c6310fd9f72eba365b3c5da))
* adding management packages ([bfed9a7](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/bfed9a762efec4c3bd713e77f17f122838b15f71))
* adding missing publish npm task to semantic-release ([c86c20e](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/c86c20e31607364da6f3981e9a1bb112c7312654))
* adding NPM publish to semantic release ([1a3a39d](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/1a3a39d5e769e8779057177963b1532d665fbd53))
* adding package management ([8c81331](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/8c813312c8d3993041feaffe2f2b9c72af31390d))
* adding package management ([bdf414b](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/bdf414bbcdb7b54810db3ef267d33027f94f51a2))
* adding package mangagement ([90691f2](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/90691f2ebe2239ea7fbfb71e773a3712ff7f654e))
* cleaning up the build process ([d67d205](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/d67d205491124a2a5c55110d4768ad174cd26682))
* correcting mistakes from monorepo merging and refactoring ([7d44c63](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/7d44c632a51a05a69bb1cd8dbb81a8b603a6d0d1))
* do not include tests in index.d.ts ([1bedf00](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/1bedf00f8dd150174e2a371ccd129d508aa0363f))
* explict alignment and word break for p, i, em, b, and strong ([8c1b93c](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/8c1b93cc9329d6ebdef558b5e31ef309214414d5))
* fixed parameters for yaml-front-matter calls ([f3d3868](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/f3d3868a87a13dbd3cc596f56d329e44929b8180))
* fixing bad package.json entry points ([af4b802](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/af4b80263634b100fceae877644bea706b2567c0))
* fixing issues with packages and paths ([db2c692](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/db2c6921951874eaf4a7f3eb1b2cc88c5a75f7f8))
* fixing main inside package.json ([8413f72](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/8413f726f74fbc03b89b6e4d91158eeb67ceea8e))
* fixing package references ([ecca99e](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/ecca99efe89f0d69b10f8607b63e9d63f2179e83))
* fixing tsconfig.json build generation ([d237c38](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/d237c38280292de249d53c11edfcc44d5232a2c1))
* fixing tsconfig.json to put files in right place ([ec9a7bc](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/ec9a7bca5999b3b9cbee6b3e5bdbeb560f02ab0e))
* moved dev deps out of deps ([635bdd1](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/635bdd175f2f87e862c2256b7e48faa919a7a9ba))
* organizing code base ([5c06222](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/5c062220058909f07be12d667cdc890aec0936b4))
* removing break-word CSS tags ([e37d58a](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/e37d58a720d85258f744e6b3e95b17c9716b7afd))
* renaming package to clarify EPUB 2 output ([c47f5cf](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/c47f5cfb2a40acd84bd8f8d0fcb30a43ce247b46))
* switched out dotted for lodash.get properly ([ae6cdd0](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/ae6cdd071b440f23ba825fb06fe71ad16f9923a2))
* updating package management ([6f19b35](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/6f19b35f9da85bea186eb968bce27ab590833ed5))
* **format:** fixing a possible undefined in cli.ts ([d562271](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/d56227147a90a07bd55cb0a0b6aa484066975236))
* switching to package management ([232f483](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/232f4837d2604995657833d21206b43dcd23d3ca))
* trying to get ci to build cleanly ([ed77f13](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/ed77f1321e2ddbf65d09a3871ef23fc8485791a2))
* updating package mangagement ([f936103](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/f936103a33b3d8e6b41a31e147226eaefe7c8e9b))
* **markdown:** do not santize input since we trust input ([5cbd85e](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/5cbd85e34986e8b26f05a23fcf29ce056cc5e1ea))


### chore

* updated to WeasyPrint 50 ([ecfa392](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/ecfa3921fdde0d46a953da0a564683c341cc4764))


### Features

* exceptions thrown while formatting return non-zero to shell ([ac94131](https://gitlab.com/mfgames-writing/mfgames-writing-js/commit/ac94131d20cfde1f87e09397bd29bcabaa9919a1))


### BREAKING CHANGES

* minimum version of WeasyPrint 50 now required
* **markdown:** HTML is no longer sanatized
