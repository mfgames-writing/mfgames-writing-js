// Figure out what package we are processing.
var fullPackageName = process.env["LERNA_PACKAGE_NAME"];
var packageName = fullPackageName.split('/')[1];
var tagFormat = packageName + "-\${version}";

// Report what package we are working with to help in debugging.
console.log("*** " + fullPackageName);

// Export the customized configuration.
module.exports = {
    extends: "semantic-release-monorepo",
    branch: "master",
    tagFormat: tagFormat,
    message: "chore(release): v${nextRelease.version} [skip ci]\n\n${nextRelease.notes}",
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/npm",
        "@semantic-release/changelog",
        "@semantic-release/git",
        "@semantic-release/changelog",
    ],
};
