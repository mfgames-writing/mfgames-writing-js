---
title: Switched to a monorepo and rehosted on Sourcehut
summary: >
  The various `mfgames-writing` packages have been combined together into a single monorepos (single repository) and integrated together.
---

Because of the difficulties of the release chain such as when `epub2` requires changes to `format` which requires changes to `contracts`, Dylan has switched the entire suite of `mfgames-writing` into a monorepo (single Git repository) using [Yarn](https://yarnpkg.com/) workspaces and [Lerna](https://lerna.js.org/). Combined with Typescript's cross-project building, it appears to have developed into a fairly simple workflow for adding features.

We are still using [sematic-release](https://github.com/semantic-release/semantic-release) to handle releases along with [Husky](https://github.com/typicode/husky) to ensure compliance on the various commits to make the release process work.

Due to not reading the code very well (e.g., not looking), the OPF and NCX packages weren't migrated into the repository but will be as a future task.

We also migrated the tiny number of tests from Mocha to Jest.

In the process, we moved the canon home from Gitlab to [Sourcehut](https://sourcehut.org/). The reasons are varied, but Dylan found the UI of Sourcehut to be easier and it has the needed features and a compatible philosophy for the project.

* [Project Page](https://sr.ht/~dmoonfire/mfgames-writing/)
* [Git](https://git.sr.ht/~dmoonfire/mfgames-writing-js)
* [Issues and Tickets](https://todo.sr.ht/~dmoonfire/mfgames-writing)
* [Feed](https://sr.ht/~dmoonfire/mfgames-writing/feed)

Overall, there are no significant changes despite the version bumps.
