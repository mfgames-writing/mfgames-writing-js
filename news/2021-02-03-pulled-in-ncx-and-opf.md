---
title: Added `@mfgames-writing/opf` and `@mfgames-writing/ncx`
summary: >
  Two of the primary package that were missed were the OPF and NCX generation library. And the project moved back to Gitlab.
---

In the process to convert mfgames-writing into a monorepo, two packages were missed. These were `@mfgames-writing/opf` and `@mfgames-writing/ncx` which are used to create the OPF and NCX files inside the EPUB files. Along the way, Dylan also normalized some of the formatting, cleaned up a bit of debris still left behind, and normalized the new packages in terms of formatting and testing.

Due to philosophical opinions, we moved the packages back to [Gitlab](https://gitlab.com/mfgames-writing/) and off Sourcehut.
